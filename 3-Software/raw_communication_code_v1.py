#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Le Raspbery Pi demande une information à l'Arduino,
# puis il affiche la réponse à l'écran

import serial  # bibliothèque permettant la communication série
import time    # pour le délai d'attente entre les messages

ser0 = serial.Serial('/dev/ttyUSB0', 115200)
ser1 = serial.Serial('/dev/ttyUSB1', 115200)
ser2 = serial.Serial('/dev/ttyUSB2', 115200)

time.sleep(2)   #on attend un peu, pour que l'Arduino soit prêt.

i=0

while 1 :
	ser0.write('a')			#on envoie le caractere 'd' au nano
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	print(ser0.readline())
	ser1.write('b')
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	print(ser1.readline())
	ser2.write('c')
	print(ser2.readline())	#on affiche la réponse
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	print(ser2.readline())
	time.sleep(1)			# on attend pendant 1 seconde
	#i+=1

print ("end of it")
