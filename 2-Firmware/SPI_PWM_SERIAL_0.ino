#// sketch by Nicolas FLORENTZ 
// internship @LAAS w. Luiz VILLA
// aug.2017 -oct 2017



#include "SPI.h" // necessary library

//-----------------------------------------variables declarations---------------------------------------------------


byte ss = 10;


byte VmiL1        =0x06;                            //char = signed 8 bits
byte VmiL2        =0x00;                            //byte = unsigned 8 bits
byte VmiH1        =0x06;                            //int = signed 16 bits
byte VmiH2        =0x40;                            //unsigned int = unsigned 16 bits
byte VmL1         =0x06;
byte VmL2         =0x80;
byte VmH1         =0x06;
byte VmH2         =0xC0;
byte VmT1_1       =0x07;
byte VmT1_2       =0x00;
byte VmT2_1       =0x07;
byte VmT2_2       =0x40;
byte VmiPgood1    =0x07;    
byte VmiPgood2    =0x80;


//declaration of the values coming from the adc.: first byte, then second byte, then concatenation of first + second

byte receivedValVmiL1;
byte receivedValVmiL2;
unsigned int receivedValVmiL =0;

byte receivedValVmiH1;
byte receivedValVmiH2;
unsigned int receivedValVmiH=0;

byte receivedValVmL1;
byte receivedValVmL2;
unsigned int receivedValVmL =0;         

byte receivedValVmH1;
byte receivedValVmH2;
unsigned int receivedValVmH=0 ;

byte receivedValVmT1_1;
byte receivedValVmT1_2;
unsigned int receivedValVmT1 =0;

byte receivedValVmT2_1;
byte receivedValVmT2_2;
unsigned int receivedValVmT2 =0;

byte receivedValPgood1;
byte receivedValPgood2;
unsigned int receivedValPgood =0;

int message =0;

void setup()
{
Serial.begin(2000000);
  
pinMode(ss, OUTPUT);   // we use this for SS pin

SPI.begin();                    // wake up the SPI bus.
SPI.setBitOrder(MSBFIRST);
SPI.setDataMode(SPI_MODE1);     // need to keep mode 1, mode 3 misses 1 bit (the MSB !!!)
                                //mode 1: slave reads orders on falling edge of clock. master reads message on falling edge
//SPI.setClockDivider(SPI_CLOCK_DIV2);


pinMode(6, OUTPUT);
    pinMode(5, OUTPUT);
 //inversion post optocoupleur => pwm high nano = pwm low high side
 //ici pwm low =pin5 sauf que inversion donc pin5 nano =pwmH => OC0B=non-inverted=> 10
 //    pwmH    =pin6                    donc pin6 nano =pwmL => OC0A= inverted => 11
 // WGM0 2:0 = 001 => mode 1:pwm phase correct cf datasheet atmega
 // CS0 2:0 =001 => no prescaling
 
    TCCR0A = 0xE1;
    TCCR0B = 0x01;


}



void loop()
{

    //--------------------------------------PWM----------------------------------------------------

    analogWrite (6, 100); //duty cycle
    analogWrite (5, 105); //duty cycle+deadtime le deadtime va sur pwmh sinon recouvrement

    //--------------------------------------Data collection-------------------------------------------


            

          //---------- aquisition of CH0

          
          digitalWrite (ss ,LOW);
                   
          SPI.transfer (VmiL1);
          receivedValVmiL1 = SPI.transfer (VmiL2);
          receivedValVmiL2 = SPI.transfer (0);
         
          digitalWrite (ss , HIGH);
          
          //---------- aquisition of CH1

          
          digitalWrite (ss , LOW);                            //puts SS (=not CS) to 0
         
          SPI.transfer (VmiH1);                               // MOSI: XXXXX, start, sgl/diff, D2    MISO XXXXXXXX                          http://avikde.me/koduino/html/class_s_p_i_class.html
          receivedValVmiH1 = SPI.transfer (VmiH2);            //MOSI :   D1, D0, XXXXX               MISO :should be XXX, null, B11, B10, B9, B8
          receivedValVmiH2 = SPI.transfer(0);                 //MOSI : XXXXXXXX                      MISO : B7, B6, B5, B4, B3, B2, B1, B0      see datasheet for more.
          
          digitalWrite (ss , HIGH);
                              
          //---------- aquisition of CH2

          
          digitalWrite (ss ,LOW);
                   
          SPI.transfer (VmL1);
          receivedValVmL1 = SPI.transfer (VmL2);
          receivedValVmL2 = SPI.transfer (0);
         
          digitalWrite (ss , HIGH);
          
          ///---------- aquisition of CH3 VmH

          
          digitalWrite (ss ,LOW);
                   
          SPI.transfer (VmH1);
          receivedValVmH1 = SPI.transfer (VmH2);
          receivedValVmH2 = SPI.transfer (0);
         
          digitalWrite (ss , HIGH);
          
          //---------- aquisition of CH4 VmT1

          
          digitalWrite (ss ,LOW);
                   
          SPI.transfer (VmT1_1);
          receivedValVmT1_1 = SPI.transfer (VmT1_2);
          receivedValVmT1_2 = SPI.transfer (0);
         
          digitalWrite (ss , HIGH);
          
          //---------- aquisition of CH5 VmT2

          
          digitalWrite (ss ,LOW);
                   
          SPI.transfer (VmT2_1);
          receivedValVmT2_1 = SPI.transfer (VmT2_2);
          receivedValVmT2_2 = SPI.transfer (0);
         
          digitalWrite (ss , HIGH);
          
          //---------- aquisition of CH6 Pgood

          
          digitalWrite (ss ,LOW);
                   
          SPI.transfer (VmiPgood1);
          receivedValPgood1 = SPI.transfer (VmiPgood2);
          receivedValPgood2 = SPI.transfer (0);
         
          digitalWrite (ss , HIGH);

         
          //-----------------------------Data processing---------------------------------------------


          //the unsigned int variable gets the first byte of data and left-shits it 12 bits so that its 4 msb get erased.
          //then unsigned int gets right-shifted 4 bits so th first byte of data is stored in the 8 msb of the unsigned int.
          //finally, the second data is simply added as the 8 lsb of the unsigned int.

          
                  
          //----------------processing VimL
          
          
          receivedValVmiL = receivedValVmiL1<<12 ;                              
          receivedValVmiL = receivedValVmiL>>4 ;                              
          
          receivedValVmiL = receivedValVmiL + receivedValVmiL2 ;  

          //temps entre 2 channels: 4,8µ
          
        
          //----------------processing VimH

          
          receivedValVmiH = receivedValVmiH1<<12;
          receivedValVmiH = receivedValVmiH >>4;
     
          receivedValVmiH = receivedValVmiH + receivedValVmiH2 ;
                    
       
          //----------------processing VmL 

          
          receivedValVmL = receivedValVmL1<<12;  
          receivedValVmL = receivedValVmL>>4 ;  
          
          receivedValVmL = receivedValVmL + receivedValVmL2 ;
                  
        
          //----------------processing VmH

          
          receivedValVmH = receivedValVmH1<<12;  
          receivedValVmH = receivedValVmH>>4 ;  
          
          receivedValVmH = receivedValVmH + receivedValVmH2 ;
                  
       
          //----------------processing VmT1 

          
          receivedValVmT1 = receivedValVmT1_1<<12 ;  
          receivedValVmT1 = receivedValVmT1>>4 ;  
          
          receivedValVmT1= receivedValVmT1 + receivedValVmT1_2;
                  
 
          //----------------processing VmT2 

          
          receivedValVmT2 = receivedValVmT2_1<<12 ;  
          receivedValVmT2 = receivedValVmT2>>4 ;  
          
          receivedValVmT2= receivedValVmT2 + receivedValVmT2_2 ;
                  
      
          //-----------------processing Pgood

          
          receivedValPgood = receivedValPgood1<<12 ;  
          receivedValPgood = receivedValPgood>>4 ;  
          
          receivedValPgood = receivedValPgood + receivedValPgood2 ;
                 
                  
          //---------talking to Raspberry---------------


if (Serial.available())  
  {
    message = Serial.read();  // on soustrait le caractère 0, qui vaut 48 en ASCII

    if (message == 'a')
    {
                Serial.print ('\n');
                Serial.println ("Nano nr 0.");
                Serial.print ('\n');
                Serial.println ("VimL:");     
                Serial.println (receivedValVmiL, DEC);
                Serial.println ("VimH:");
                Serial.println (receivedValVmiH, DEC);
                Serial.println ("VmL:");
                Serial.println (receivedValVmL, DEC);
                Serial.println ("VmH:");
                Serial.println (receivedValVmH, DEC);
                Serial.println ("VmT1:");
                Serial.println (receivedValVmT1, DEC);
                Serial.println ("Pgood:");
                Serial.println (receivedValPgood, DEC);

    }
  }

}
    

