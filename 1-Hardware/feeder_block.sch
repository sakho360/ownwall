EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Power_converter_V4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 7
Title ""
Date "20 apr 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 850  650  0    60   ~ 0
BLOCK INPUTS
Text Notes 850  800  0    60   ~ 0
----------------------------
Text Notes 8250 700  0    60   ~ 0
BLOCK OUTPUTS
Text Notes 8250 850  0    60   ~ 0
----------------------------
Text Notes 3650 1350 0    60   ~ 0
This is the global feeder circuit. \nIn this solution, the feeder is composed of a DC-DC\nconverter that regulates 15V out of the Vlow. \nThese 15V are used to create 5V for the high side\nand 5V for the low side.\nThe masses are separated.
Text Notes 3650 700  0    60   ~ 0
FEEDER DETAILS
Text Notes 3650 800  0    60   ~ 0
-----------------------------------------------
Text HLabel 1050 950  0    60   Input ~ 0
Vlow
Text HLabel 1050 1150 0    60   Input ~ 0
Vhigh
Text Label 1650 1150 2    60   ~ 0
Vhigh
Text Label 1650 950  2    60   ~ 0
Vlow
Text HLabel 1050 1400 0    60   Input ~ 0
GNDPwR
Text Label 1650 1400 2    60   ~ 0
GNDPwR
Text HLabel 8950 1000 2    60   Output ~ 0
+5VLow
Text Label 8300 1000 0    60   ~ 0
+5VLow
Text HLabel 8950 1150 2    60   Output ~ 0
GNDLow
Text Label 8300 1150 0    60   ~ 0
GNDLow
Text Notes 8700 4850 1    60   ~ 0
WARNING: MASSES MUST BE SEPARATED!!!
Text HLabel 10400 1150 2    60   Output ~ 0
+5VHigh
Text Label 9750 1150 0    60   ~ 0
+5VHigh
Text HLabel 10400 1000 2    60   Output ~ 0
+15VHigh
Text Label 9750 1000 0    60   ~ 0
+15VHigh
Text HLabel 10400 1300 2    60   Output ~ 0
GNDPwR
Text Label 9750 1300 0    60   ~ 0
GNDPwR
Text Notes 900  3250 0    60   ~ 0
DC-DC converter \n------------------------------\nVin 4.5 to 60V // Vout 15V, 1A\n---INDUCTOR AND CURRENT---\nInductor = 12LRS333C (Murata)\nDiL = 0.5A\n---SETUP-ELEMENTS---\nFB = 0.75V (Potentiometer for bettter precision)\nRT = 33k (for an operation around 700kHz)\nRpg = 33k (pull-up which sends the "power good" signal to the Arduino)\n--CAPACITORS-VOLTAGE----\nCH1 = 20u (should hold up to 100V, thin-film) \nCH2 = 4.7u (should hold up to 100V, ceramic)\nCH3 = 20u (150% bigger than the minimum)\nCb = 100n (ceramic, X7R, for good thermal stability)
$Comp
L CP CH6
U 1 1 59765087
P 6150 4900
F 0 "CH6" H 6175 5000 50  0000 L CNN
F 1 "20u" H 6175 4800 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 6188 4750 50  0001 C CNN
F 3 "" H 6150 4900 50  0000 C CNN
	1    6150 4900
	1    0    0    -1  
$EndComp
$Comp
L R RH2
U 1 1 597664C7
P 5550 5150
F 0 "RH2" V 5630 5150 50  0000 C CNN
F 1 "10k" V 5550 5150 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 5480 5150 50  0001 C CNN
F 3 "" H 5550 5150 50  0000 C CNN
	1    5550 5150
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-Power_converter_V4 LDH2
U 1 1 597664CD
P 5550 4700
F 0 "LDH2" H 5550 4800 50  0000 C CNN
F 1 "LED" H 5550 4600 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 5550 4700 50  0001 C CNN
F 3 "" H 5550 4700 50  0000 C CNN
	1    5550 4700
	0    -1   -1   0   
$EndComp
$Comp
L LMR16010 Uf1
U 1 1 5976D566
P 3050 4450
F 0 "Uf1" H 2605 4185 60  0000 C CNN
F 1 "LMR16010" H 3650 4150 60  0000 C CNN
F 2 "test:SOIC-8-1EP_3.9x4.9mm_Pitch1.27mm_thermal++" H 2935 4380 60  0001 C CNN
F 3 "" H 2935 4380 60  0000 C CNN
	1    3050 4450
	0    1    1    0   
$EndComp
$Comp
L C Cb1
U 1 1 5976EAE8
P 3950 4150
F 0 "Cb1" H 3975 4250 50  0000 L CNN
F 1 "100n" H 3975 4050 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 3988 4000 50  0001 C CNN
F 3 "" H 3950 4150 50  0000 C CNN
	1    3950 4150
	0    -1   -1   0   
$EndComp
$Comp
L L Ls1
U 1 1 5976F377
P 4450 4450
F 0 "Ls1" V 4400 4450 50  0000 C CNN
F 1 "33u" V 4525 4450 50  0000 C CNN
F 2 "digital:inductance_murata_1200LRS" H 4450 4450 50  0001 C CNN
F 3 "" H 4450 4450 50  0000 C CNN
	1    4450 4450
	0    -1   -1   0   
$EndComp
$Comp
L D_Schottky Ds1
U 1 1 5976F8F9
P 3950 4700
F 0 "Ds1" H 3950 4800 50  0000 C CNN
F 1 "D_Schottky" H 3950 4600 50  0000 C CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 3950 4700 50  0001 C CNN
F 3 "" H 3950 4700 50  0000 C CNN
	1    3950 4700
	0    1    1    0   
$EndComp
$Comp
L R Rt1
U 1 1 597713E8
P 2200 5000
F 0 "Rt1" V 2280 5000 50  0000 C CNN
F 1 "33k" V 2200 5000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 2130 5000 50  0001 C CNN
F 3 "" H 2200 5000 50  0000 C CNN
	1    2200 5000
	1    0    0    -1  
$EndComp
$Comp
L R Rfbt1
U 1 1 59771C86
P 4850 4650
F 0 "Rfbt1" V 4930 4650 50  0000 C CNN
F 1 "100k" V 4850 4650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4780 4650 50  0001 C CNN
F 3 "" H 4850 4650 50  0000 C CNN
	1    4850 4650
	1    0    0    -1  
$EndComp
$Comp
L R Rffb1
U 1 1 59771EE1
P 4850 5550
F 0 "Rffb1" V 4930 5550 50  0000 C CNN
F 1 "4k7" V 4850 5550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4780 5550 50  0001 C CNN
F 3 "" H 4850 5550 50  0000 C CNN
	1    4850 5550
	1    0    0    -1  
$EndComp
Text Label 825  4250 2    60   ~ 0
Vlow
Text Label 1300 5750 2    60   ~ 0
GNDPwR
Text Label 4350 5000 2    60   ~ 0
GNDPwR
Text Label 5400 4000 2    60   ~ 0
+15VHigh
$Comp
L TME/CRE Uf3
U 1 1 59777E0C
P 8650 3950
F 0 "Uf3" H 8200 4400 60  0000 C CNN
F 1 "TME/CRE" H 9050 4400 60  0000 C CNN
F 2 "test:TME_0505s" H 8535 3880 60  0001 C CNN
F 3 "http://www.tme.eu/en/Document/ecc45a2bcbf6e1314ca25a1f4efc1d56/TME-SERIES-EN.pdf" H 8535 3880 60  0001 C CNN
	1    8650 3950
	1    0    0    -1  
$EndComp
Text Notes 2100 6350 0    60   ~ 0
DC linear regulator for 5V supply on the high side
Text Label 2150 6800 0    60   ~ 0
+15VHigh
Text Label 2950 7650 2    60   ~ 0
GNDPwR
Text Label 4800 6800 0    60   ~ 0
+5VHigh
Text Label 7850 5200 0    60   ~ 0
+5VHigh
Text Label 7675 4625 0    60   ~ 0
GNDPwR
Text Label 10100 5050 2    60   ~ 0
GNDLow
$Comp
L R RL1
U 1 1 572042AC
P 10800 5300
F 0 "RL1" V 10880 5300 50  0000 C CNN
F 1 "10k" V 10800 5300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 10730 5300 50  0001 C CNN
F 3 "" H 10800 5300 50  0000 C CNN
	1    10800 5300
	1    0    0    -1  
$EndComp
Text Notes 9600 2050 0    60   ~ 0
LOW VOLTAGE SIDE FEEDER
Text Notes 8850 2200 0    60   ~ 0
----------------------------
Text Notes 9200 2600 0    60   ~ 0
This component generates \n+5V for the low voltage side reference. \nThe footprint is compatible with both\nMurata and Traco components.\n
$Comp
L LED-RESCUE-Power_converter_V4 LDL1
U 1 1 595BC656
P 10800 4900
F 0 "LDL1" H 10800 5000 50  0000 C CNN
F 1 "LED" H 10800 4800 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 10800 4900 50  0001 C CNN
F 3 "" H 10800 4900 50  0000 C CNN
	1    10800 4900
	0    -1   -1   0   
$EndComp
Text Label 10800 5750 2    60   ~ 0
GNDLow
Text Label 11100 4600 2    60   ~ 0
+5VLow
$Comp
L POT-RESCUE-Power_converter_V4 PH1
U 1 1 59787B66
P 4850 5150
F 0 "PH1" V 4750 5150 50  0000 C CNN
F 1 "1k" V 4850 5150 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Bourns_3296Y" H 4850 5150 50  0001 C CNN
F 3 "" H 4850 5150 50  0000 C CNN
	1    4850 5150
	1    0    0    -1  
$EndComp
$Comp
L CP CH1
U 1 1 5978D844
P 1400 4450
F 0 "CH1" H 1425 4550 50  0000 L CNN
F 1 "20u" H 1425 4350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 1438 4300 50  0001 C CNN
F 3 "" H 1400 4450 50  0000 C CNN
	1    1400 4450
	1    0    0    -1  
$EndComp
$Comp
L C CH2
U 1 1 5978E38C
P 1800 4450
F 0 "CH2" H 1825 4550 50  0000 L CNN
F 1 "4.7u" H 1825 4350 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 1838 4300 50  0001 C CNN
F 3 "" H 1800 4450 50  0000 C CNN
	1    1800 4450
	1    0    0    -1  
$EndComp
Text HLabel 8950 1300 2    60   Output ~ 0
Pgood
Text Label 8300 1300 0    60   ~ 0
Pgood
Text Label 2650 5450 0    60   ~ 0
Pgood
$Comp
L LED-RESCUE-Power_converter_V4 LDH1
U 1 1 599EACA5
P 4500 7050
F 0 "LDH1" H 4500 7150 50  0000 C CNN
F 1 "LED" H 4500 6950 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 4500 7050 50  0001 C CNN
F 3 "" H 4500 7050 50  0000 C CNN
	1    4500 7050
	0    -1   -1   0   
$EndComp
$Comp
L R RH1
U 1 1 599EB330
P 4500 7450
F 0 "RH1" V 4600 7450 50  0000 C CNN
F 1 "2K2" V 4500 7450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4430 7450 50  0001 C CNN
F 3 "" H 4500 7450 50  0000 C CNN
	1    4500 7450
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 TH1
U 1 1 599F0400
P 6200 2350
F 0 "TH1" H 6200 2450 50  0000 C CNN
F 1 "CON1X1" V 6300 2400 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6200 2350 50  0001 C CNN
F 3 "" H 6200 2350 50  0000 C CNN
	1    6200 2350
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TH2
U 1 1 599F047A
P 6200 2600
F 0 "TH2" H 6200 2700 50  0000 C CNN
F 1 "CON1X1" V 6300 2600 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6200 2600 50  0001 C CNN
F 3 "" H 6200 2600 50  0000 C CNN
	1    6200 2600
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TH3
U 1 1 599F0516
P 6200 2850
F 0 "TH3" H 6200 2950 50  0000 C CNN
F 1 "CON1X1" V 6300 2850 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6200 2850 50  0001 C CNN
F 3 "" H 6200 2850 50  0000 C CNN
	1    6200 2850
	-1   0    0    1   
$EndComp
Text Label 6650 2350 0    60   ~ 0
+15VHigh
Text Label 6650 2600 0    60   ~ 0
+5VHigh
Text Label 6650 2850 0    60   ~ 0
+5VLow
Wire Wire Line
	4500 7200 4500 7300
Connection ~ 4500 6800
Wire Wire Line
	4500 6800 4500 6900
Connection ~ 2650 6800
Wire Wire Line
	2650 6800 2650 6950
Connection ~ 1800 5750
Wire Wire Line
	1800 5750 1800 4600
Connection ~ 1800 3750
Wire Wire Line
	1800 3750 1800 4300
Connection ~ 1400 5750
Wire Wire Line
	1400 4600 1400 5750
Connection ~ 1400 3750
Wire Wire Line
	1400 3750 1400 4300
Wire Wire Line
	8950 1300 8300 1300
Wire Wire Line
	2500 5450 2650 5450
Wire Wire Line
	2500 4750 2500 5450
Wire Wire Line
	2650 4750 2500 4750
Connection ~ 5550 4450
Wire Wire Line
	6150 4450 6150 4750
Connection ~ 5550 5750
Wire Wire Line
	6150 5750 6150 5050
Connection ~ 4850 4850
Wire Wire Line
	4600 4850 4850 4850
Wire Wire Line
	4600 5350 4600 4850
Wire Wire Line
	3700 5350 4600 5350
Wire Wire Line
	3700 4750 3700 5350
Connection ~ 4850 4950
Wire Wire Line
	4850 4950 5000 4950
Wire Wire Line
	5000 4950 5000 5150
Wire Wire Line
	4850 5400 4850 5300
Connection ~ 4850 5750
Wire Wire Line
	4850 5750 4850 5700
Connection ~ 3150 5750
Connection ~ 2200 5750
Wire Wire Line
	1300 5750 6150 5750
Wire Wire Line
	4850 4800 4850 5000
Connection ~ 10800 4600
Wire Wire Line
	10800 4600 10800 4750
Wire Wire Line
	10800 5050 10800 5150
Wire Wire Line
	10800 5450 10800 5750
Wire Notes Line
	7200 1900 7200 6350
Wire Notes Line
	650  5900 6750 5900
Wire Wire Line
	8900 5050 10100 5050
Wire Wire Line
	8900 4350 8900 5050
Wire Wire Line
	9050 4600 11100 4600
Wire Wire Line
	9050 4350 9050 4600
Wire Wire Line
	8250 4625 7675 4625
Wire Wire Line
	8250 4350 8250 4625
Wire Wire Line
	8400 5200 7850 5200
Wire Wire Line
	8400 4350 8400 5200
Wire Notes Line
	8750 2300 8750 5950
Wire Notes Line
	8550 5950 8550 2300
Wire Wire Line
	3150 7650 2950 7650
Connection ~ 4000 6800
Connection ~ 3700 6800
Wire Wire Line
	3450 6800 4800 6800
Wire Wire Line
	2150 6800 2850 6800
Wire Wire Line
	3700 6800 3700 6950
Connection ~ 5200 4450
Wire Wire Line
	5200 4000 5400 4000
Wire Wire Line
	5200 4450 5200 4000
Wire Wire Line
	3950 5000 4350 5000
Wire Wire Line
	5550 5750 5550 5300
Wire Wire Line
	5550 4450 5550 4550
Wire Wire Line
	3150 5150 3150 5750
Connection ~ 4850 4450
Wire Wire Line
	4850 4500 4850 4450
Wire Wire Line
	4600 4450 6150 4450
Wire Wire Line
	3650 4750 3700 4750
Wire Wire Line
	2200 5150 2200 5750
Wire Wire Line
	2200 4450 2200 4850
Wire Wire Line
	2650 4450 2200 4450
Connection ~ 2650 3750
Wire Wire Line
	2650 3750 2650 4150
Wire Wire Line
	1325 3750 3150 3750
Connection ~ 4250 4450
Wire Wire Line
	4250 4450 4250 4150
Wire Wire Line
	4250 4150 4100 4150
Wire Wire Line
	3650 4150 3800 4150
Wire Wire Line
	3950 4850 3950 5000
Connection ~ 3950 4450
Wire Wire Line
	3950 4550 3950 4450
Wire Wire Line
	3650 4450 4300 4450
Wire Wire Line
	5550 4850 5550 5000
Wire Wire Line
	10400 1300 9750 1300
Wire Wire Line
	10400 1000 9750 1000
Wire Wire Line
	10400 1150 9750 1150
Wire Wire Line
	8950 1150 8300 1150
Wire Wire Line
	8950 1000 8300 1000
Wire Wire Line
	1050 1400 1650 1400
Wire Wire Line
	1050 1150 1650 1150
Wire Wire Line
	1050 950  1650 950 
Wire Notes Line
	8050 1800 8050 700 
Wire Notes Line
	8050 1800 10900 1800
Wire Notes Line
	3500 1750 3500 650 
Wire Notes Line
	650  1750 3500 1750
Wire Wire Line
	6400 2350 6650 2350
Wire Wire Line
	6400 2600 6650 2600
Wire Wire Line
	6400 2850 6650 2850
Wire Wire Line
	3150 7000 3150 7650
Wire Wire Line
	2650 7150 2650 7300
Connection ~ 3150 7300
Wire Wire Line
	3700 7150 3700 7300
Connection ~ 3700 7300
Wire Wire Line
	3850 7600 4500 7600
Wire Wire Line
	3850 7300 3850 7600
Connection ~ 3850 7300
Text Label 800  3375 2    60   ~ 0
Vhigh
Wire Wire Line
	825  4250 1125 4250
Wire Wire Line
	1125 4250 1125 4050
Wire Wire Line
	800  3375 1125 3375
Wire Wire Line
	1125 3375 1125 3450
$Comp
L D_Schottky_x2_KCom_AAK D1
U 1 1 59DCDCC2
P 1125 3750
F 0 "D1" H 1175 3650 50  0000 C CNN
F 1 "D_Schottky_x2_KCom_AAK" H 1125 3850 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 1125 3750 50  0001 C CNN
F 3 "" H 1125 3750 50  0001 C CNN
	1    1125 3750
	0    -1   -1   0   
$EndComp
$Comp
L MCP1700 U5
U 1 1 59E04A33
P 3150 6800
F 0 "U5" H 3250 6650 50  0000 C CNN
F 1 "MCP1700" H 3150 6950 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:TO-252-2" H 3150 6800 50  0001 C CNN
F 3 "" H 3150 6800 50  0000 C CNN
	1    3150 6800
	1    0    0    -1  
$EndComp
$Comp
L C_Small C1
U 1 1 59E04A8C
P 2650 7050
F 0 "C1" H 2660 7120 50  0000 L CNN
F 1 "C_Small" H 2660 6970 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2650 7050 50  0001 C CNN
F 3 "" H 2650 7050 50  0001 C CNN
	1    2650 7050
	1    0    0    -1  
$EndComp
$Comp
L C_Small C2
U 1 1 59E04B25
P 3700 7050
F 0 "C2" H 3710 7120 50  0000 L CNN
F 1 "C_Small" H 3710 6970 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 3700 7050 50  0001 C CNN
F 3 "" H 3700 7050 50  0001 C CNN
	1    3700 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 7300 3850 7300
$EndSCHEMATC
