EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Power_converter_V4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title ""
Date "20 apr 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5250 2800 1    60   ~ 0
VgH
Text Label 3350 4450 2    60   ~ 0
VgL
Text Label 4900 2800 1    60   ~ 0
VsH
Text Label 3375 4850 2    60   ~ 0
VsL
Text Label 1250 3600 2    60   ~ 0
Vlow
Text Label 7950 3600 2    60   ~ 0
Vhigh
Text Label 10250 6300 2    60   ~ 0
ViH-
Text Label 9650 6300 2    60   ~ 0
ViH+
Text Label 1150 4700 2    60   ~ 0
VL+
Text Label 1300 5350 2    60   ~ 0
VL-
Text Label 7650 5150 0    60   ~ 0
VH-
$Comp
L IRF540N-RESCUE-Power_converter_V4 MH1
U 1 1 57304D53
P 5300 3500
F 0 "MH1" V 5650 3550 50  0000 L CNN
F 1 "FDPF39N20" V 5550 3250 50  0000 L CNN
F 2 "test:TO-220_Horizontal_inversé" H 5550 3425 50  0001 L CIN
F 3 "" H 5300 3500 50  0000 L CNN
	1    5300 3500
	0    1    1    0   
$EndComp
$Comp
L IRF540N-RESCUE-Power_converter_V4 ML1
U 1 1 573050C4
P 4025 4400
F 0 "ML1" H 4225 4100 50  0000 L CNN
F 1 "FDPF39N20" V 4300 4200 50  0000 L CNN
F 2 "test:TO-220_Horizontal_inversé" H 4275 4325 50  0001 L CIN
F 3 "" H 4025 4400 50  0000 L CNN
	1    4025 4400
	1    0    0    -1  
$EndComp
$Comp
L R RM1
U 1 1 592DA79D
P 5250 3075
F 0 "RM1" V 5330 3075 50  0000 C CNN
F 1 "2R2" V 5250 3075 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 5180 3075 50  0001 C CNN
F 3 "" H 5250 3075 50  0000 C CNN
	1    5250 3075
	-1   0    0    1   
$EndComp
$Comp
L R RM2
U 1 1 592DAAA9
P 3600 4450
F 0 "RM2" V 3700 4450 50  0000 C CNN
F 1 "2R2" V 3600 4450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 3530 4450 50  0001 C CNN
F 3 "" H 3600 4450 50  0000 C CNN
	1    3600 4450
	0    1    1    0   
$EndComp
$Comp
L L L1
U 1 1 592DC284
P 2600 3600
F 0 "L1" V 2550 3600 50  0000 C CNN
F 1 "Inductor" V 2700 3600 50  0000 C CNN
F 2 "7805:MURATA_Inductor_Big" H 2600 3600 50  0001 C CNN
F 3 "" H 2600 3600 50  0000 C CNN
	1    2600 3600
	0    -1   -1   0   
$EndComp
$Comp
L CP1 CPL2
U 1 1 592EA0B9
P 2100 4450
F 0 "CPL2" H 2125 4550 50  0000 L CNN
F 1 "C" H 2125 4350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 2100 4450 50  0001 C CNN
F 3 "" H 2100 4450 50  0000 C CNN
	1    2100 4450
	1    0    0    -1  
$EndComp
$Comp
L R RVL1
U 1 1 592EA443
P 1400 3900
F 0 "RVL1" V 1300 3900 50  0000 C CNN
F 1 "470K" V 1400 3900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 1330 3900 50  0001 C CNN
F 3 "" H 1400 3900 50  0000 C CNN
	1    1400 3900
	1    0    0    -1  
$EndComp
$Comp
L R RVL2
U 1 1 592EA4CD
P 1400 4950
F 0 "RVL2" V 1300 4950 50  0000 C CNN
F 1 "4K7" V 1400 4950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 1330 4950 50  0001 C CNN
F 3 "" H 1400 4950 50  0000 C CNN
	1    1400 4950
	1    0    0    -1  
$EndComp
$Comp
L D_ALT D2
U 1 1 592ED1C4
P 5250 4075
F 0 "D2" V 5200 4275 50  0000 C CNN
F 1 "Diode" V 5300 4275 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-220-2_Horizontal_Reversed" H 5250 4075 50  0001 C CNN
F 3 "" H 5250 4075 50  0000 C CNN
	1    5250 4075
	-1   0    0    1   
$EndComp
$Comp
L D_ALT D3
U 1 1 592ED23B
P 4525 4400
F 0 "D3" V 4475 4600 50  0000 C CNN
F 1 "Diode" V 4575 4600 50  0000 C CNN
F 2 "TO_SOT_Packages_THT:TO-220-2_Horizontal_Reversed" H 4525 4400 50  0001 C CNN
F 3 "" H 4525 4400 50  0000 C CNN
	1    4525 4400
	0    1    1    0   
$EndComp
$Comp
L CP1 CPH3
U 1 1 592ED805
P 6450 4925
F 0 "CPH3" H 6475 5025 50  0000 L CNN
F 1 "C" H 6475 4825 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 6450 4925 50  0001 C CNN
F 3 "" H 6450 4925 50  0000 C CNN
	1    6450 4925
	1    0    0    -1  
$EndComp
$Comp
L R RVH2
U 1 1 592ED905
P 7300 4900
F 0 "RVH2" V 7200 4900 50  0000 C CNN
F 1 "4K7" V 7300 4900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 7230 4900 50  0001 C CNN
F 3 "" H 7300 4900 50  0000 C CNN
	1    7300 4900
	1    0    0    -1  
$EndComp
$Comp
L R RVH1
U 1 1 592ED98E
P 7300 4025
F 0 "RVH1" V 7200 4025 50  0000 C CNN
F 1 "470K" V 7300 4025 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 7230 4025 50  0001 C CNN
F 3 "" H 7300 4025 50  0000 C CNN
	1    7300 4025
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02-RESCUE-Power_converter_V4 CNH1
U 1 1 59301D89
P 10550 5400
F 0 "CNH1" H 10750 5450 50  0000 C CNN
F 1 "High" H 10750 5350 50  0000 C CNN
F 2 "test:PhoenixContact_FFKDS-V1-5.08_02x7.62mm" H 10550 5400 50  0001 C CNN
F 3 "" H 10550 5400 50  0000 C CNN
	1    10550 5400
	1    0    0    -1  
$EndComp
$Comp
L R RiL1
U 1 1 5955136C
P 9800 4350
F 0 "RiL1" V 9880 4350 50  0000 C CNN
F 1 "0.02" V 9800 4350 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 9730 4350 50  0001 C CNN
F 3 "" H 9800 4350 50  0000 C CNN
	1    9800 4350
	0    1    1    0   
$EndComp
$Comp
L R RiH1
U 1 1 595513EC
P 9950 5450
F 0 "RiH1" V 10030 5450 50  0000 C CNN
F 1 "0.02" V 9950 5450 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 9880 5450 50  0001 C CNN
F 3 "" H 9950 5450 50  0000 C CNN
	1    9950 5450
	0    1    1    0   
$EndComp
$Comp
L LM35-LP S1
U 1 1 595B64EF
P 3850 6450
F 0 "S1" H 3600 6700 50  0000 C CNN
F 1 "LM35DZ" H 3900 6700 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Inline_Wide" H 3900 6200 50  0001 L CNN
F 3 "" H 3850 6450 50  0000 C CNN
	1    3850 6450
	1    0    0    -1  
$EndComp
Text Notes 900  600  0    60   ~ 0
BLOCK INPUTS
Text Notes 6950 650  0    60   ~ 0
BLOCK OUTPUTS
Text HLabel 1050 850  0    60   Input ~ 0
VgH
Text HLabel 1050 1050 0    60   Input ~ 0
VsH
Text HLabel 1050 1250 0    60   Input ~ 0
VgL
Text HLabel 1050 1450 0    60   Input ~ 0
VsL
Text Label 1850 850  0    60   ~ 0
VgH
Text Label 1850 1250 0    60   ~ 0
VgL
Text Label 1850 1050 0    60   ~ 0
VsH
Text Label 1850 1450 0    60   ~ 0
VsL
Text Notes 9450 4050 0    60   ~ 0
ENERGY CONNECTORS
Text Label 9300 4250 0    60   ~ 0
Vlow
Text Label 9500 5350 0    60   ~ 0
Vhigh
Text Notes 700  2000 0    60   ~ 0
CONVERTER INPUT
Text Notes 650  2150 0    60   ~ 0
----------------------------
Text Label 9500 5150 2    60   ~ 0
ViL-
Text HLabel 7550 1350 2    60   Output ~ 0
ViH+
Text HLabel 7550 1550 2    60   Output ~ 0
ViH-
Text Label 7600 4625 0    60   ~ 0
VH+
Text Notes 700  2600 0    60   ~ 0
The inputs are the gate-source \nvoltages for the high-side (H) \nand low-side (L) transistors. \n(VgH-VsH/VgL-VsL)\n
Text Notes 8900 3900 0    60   ~ 0
There are 4 measurements spread\namong the power elements:\n\nViL+ and ViL- is the voltage drop \nof the shunt resistance reading low side current\nEQUIVALECE: 1 Amp = 20mV \n\nViH+ and ViH- is the voltage drop \nof the shunt resistance reading high side current\nEQUIVALECE: 1 Amp = 20mV \n\nVL+ and VL- is the voltage drop \nof the resistance reading low side voltage\nEQUIVALECE: 100V = 0.33V to 1V \n\nVH+ and VH- is the voltage drop \nof the resistance reading high side voltage\nEQUIVALECE: 200V = 0.66V to 2V \n 
Text Notes 8800 1950 0    60   ~ 0
MEASUREMENTS
Text Notes 8800 2050 0    60   ~ 0
-------------------------
Text Notes 2800 1200 0    60   ~ 0
This topology employs capacitors\nboth at the input and the output. \nThe inductance in the middle allows it\nto be a fully reversible buck-boost.
Text Notes 2900 650  0    60   ~ 0
DETAILS OF THE POWER BLOCK
Text Notes 2900 750  0    60   ~ 0
-----------------------------------------------
Text Notes 4650 1500 0    60   ~ 0
This version of the converter uses 200V\nMOSFETS capable of handling up to 20A. \nThis converter was designed to have an \noutput up to 120V making its input \nreasonable up to 60V.\nThe power in this structure should \nNOT go beyond 500W. 
Text HLabel 8650 1150 2    60   Output ~ 0
VL-
Text HLabel 8650 1350 2    60   Output ~ 0
VH+
Text HLabel 8650 1550 2    60   Output ~ 0
VH-
Text Label 6900 1550 0    60   ~ 0
ViH-
Text Label 6900 1350 0    60   ~ 0
ViH+
Text Label 8050 1350 0    60   ~ 0
VH+
Text Label 8050 1550 0    60   ~ 0
VH-
Text Label 8050 1150 0    60   ~ 0
VL-
Text HLabel 10600 900  2    60   Output ~ 0
Vlow
Text HLabel 10600 1100 2    60   Output ~ 0
Vhigh
Text Label 10250 1100 0    60   ~ 0
Vhigh
Text Label 10250 900  0    60   ~ 0
Vlow
$Comp
L CONN_01X02-RESCUE-Power_converter_V4 CNL1
U 1 1 595D128A
P 10450 4300
F 0 "CNL1" H 10650 4350 50  0000 C CNN
F 1 "Low" H 10650 4250 50  0000 C CNN
F 2 "test:PhoenixContact_FFKDS-V1-5.08_02x7.62mm" H 10450 4300 50  0001 C CNN
F 3 "" H 10450 4300 50  0000 C CNN
	1    10450 4300
	1    0    0    -1  
$EndComp
Text Label 2675 6000 0    60   ~ 0
+5VHigh
Text Notes 5450 5800 0    60   ~ 0
TEMPERATURE SENSORS
Text Label 4600 6450 0    60   ~ 0
VT1+
Text Label 4600 7175 0    60   ~ 0
VT1-
Text Label 9100 950  0    60   ~ 0
VT1+
Text Label 9100 1150 0    60   ~ 0
VT1-
Text HLabel 9650 950  2    60   Output ~ 0
VT1+
Text HLabel 9650 1150 2    60   Output ~ 0
VT1-
$Comp
L POT-RESCUE-Power_converter_V4 PVL1
U 1 1 595D639B
P 1400 4450
F 0 "PVL1" V 1300 4450 50  0000 C CNN
F 1 "POT" V 1400 4450 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Bourns_3296Y" H 1400 4450 50  0001 C CNN
F 3 "" H 1400 4450 50  0000 C CNN
	1    1400 4450
	1    0    0    -1  
$EndComp
$Comp
L POT-RESCUE-Power_converter_V4 PVH1
U 1 1 595D6623
P 7300 4425
F 0 "PVH1" V 7200 4425 50  0000 C CNN
F 1 "POT" V 7300 4425 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Bourns_3296Y" H 7300 4425 50  0001 C CNN
F 3 "" H 7300 4425 50  0000 C CNN
	1    7300 4425
	1    0    0    -1  
$EndComp
Text HLabel 1050 1600 0    60   Input ~ 0
+5VHigh
Text Label 1850 1600 0    60   ~ 0
+5VHigh
$Comp
L C Cdtemp1
U 1 1 596E2E43
P 3250 6750
F 0 "Cdtemp1" H 3275 6850 50  0000 L CNN
F 1 "0.1uF" H 3275 6650 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 3288 6600 50  0001 C CNN
F 3 "" H 3250 6750 50  0000 C CNN
	1    3250 6750
	1    0    0    -1  
$EndComp
Text Label 9200 4550 0    60   ~ 0
GNDPwR
Text Label 9500 5650 2    60   ~ 0
GNDPwR
Text HLabel 10550 1500 2    60   Output ~ 0
GNDPwR
Text Label 6900 950  0    60   ~ 0
ViL+
Text Label 6900 1150 0    60   ~ 0
ViL-
Text HLabel 7550 1150 2    60   Output ~ 0
ViL-
Text HLabel 7550 950  2    60   Output ~ 0
ViL+
Text Label 10100 5150 0    60   ~ 0
ViL+
$Comp
L CP1 CPL1
U 1 1 599FEEC6
P 1750 4450
F 0 "CPL1" H 1775 4550 50  0000 L CNN
F 1 "C" H 1775 4350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 1750 4450 50  0001 C CNN
F 3 "" H 1750 4450 50  0000 C CNN
	1    1750 4450
	1    0    0    -1  
$EndComp
$Comp
L CP1 CPL3
U 1 1 599FF056
P 2450 4450
F 0 "CPL3" H 2475 4550 50  0000 L CNN
F 1 "C" H 2475 4350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 2450 4450 50  0001 C CNN
F 3 "" H 2450 4450 50  0000 C CNN
	1    2450 4450
	1    0    0    -1  
$EndComp
$Comp
L CP1 CPL4
U 1 1 599FF0E4
P 2800 4450
F 0 "CPL4" H 2825 4550 50  0000 L CNN
F 1 "C" H 2825 4350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 2800 4450 50  0001 C CNN
F 3 "" H 2800 4450 50  0000 C CNN
	1    2800 4450
	1    0    0    -1  
$EndComp
$Comp
L R RiL2
U 1 1 59A6B727
P 9800 4650
F 0 "RiL2" V 9880 4650 50  0000 C CNN
F 1 "0.02" V 9800 4650 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 9730 4650 50  0001 C CNN
F 3 "" H 9800 4650 50  0000 C CNN
	1    9800 4650
	0    1    1    0   
$EndComp
$Comp
L R RiL3
U 1 1 59A6B79E
P 9800 4900
F 0 "RiL3" V 9880 4900 50  0000 C CNN
F 1 "0.02" V 9800 4900 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 9730 4900 50  0001 C CNN
F 3 "" H 9800 4900 50  0000 C CNN
	1    9800 4900
	0    1    1    0   
$EndComp
$Comp
L R RiH2
U 1 1 59A6B914
P 9950 5650
F 0 "RiH2" V 10030 5650 50  0000 C CNN
F 1 "0.02" V 9950 5650 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 9880 5650 50  0001 C CNN
F 3 "" H 9950 5650 50  0000 C CNN
	1    9950 5650
	0    1    1    0   
$EndComp
$Comp
L R RiH3
U 1 1 59A6B98B
P 9950 5850
F 0 "RiH3" V 10030 5850 50  0000 C CNN
F 1 "0.02" V 9950 5850 50  0000 C CNN
F 2 "test:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal_1.2mm_drill" H 9880 5850 50  0001 C CNN
F 3 "" H 9950 5850 50  0000 C CNN
	1    9950 5850
	0    1    1    0   
$EndComp
$Comp
L CP1 CPH4
U 1 1 59A6CF5C
P 6800 4925
F 0 "CPH4" H 6825 5025 50  0000 L CNN
F 1 "C" H 6825 4825 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 6800 4925 50  0001 C CNN
F 3 "" H 6800 4925 50  0000 C CNN
	1    6800 4925
	1    0    0    -1  
$EndComp
$Comp
L CP1 CPH2
U 1 1 59B65685
P 6150 4925
F 0 "CPH2" H 6175 5025 50  0000 L CNN
F 1 "C" H 6175 4825 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 6150 4925 50  0001 C CNN
F 3 "" H 6150 4925 50  0000 C CNN
	1    6150 4925
	1    0    0    -1  
$EndComp
$Comp
L CP1 CPH1
U 1 1 59B6573D
P 5925 4925
F 0 "CPH1" H 5950 5025 50  0000 L CNN
F 1 "C" H 5950 4825 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D18.0mm_P7.50mm" H 5925 4925 50  0001 C CNN
F 3 "" H 5925 4925 50  0000 C CNN
	1    5925 4925
	1    0    0    -1  
$EndComp
Text Label 10200 1500 2    60   ~ 0
GNDPwR
Text Label 2675 5525 2    60   ~ 0
GNDPwR
Text Label 4125 7350 2    60   ~ 0
GNDPwR
Wire Notes Line
	2450 1750 2450 650 
Wire Notes Line
	6750 1750 6750 650 
Wire Wire Line
	3750 4450 3825 4450
Wire Wire Line
	5250 3300 5250 3225
Wire Wire Line
	2750 3600 5100 3600
Wire Wire Line
	4125 3600 4125 4200
Wire Wire Line
	1850 850  1050 850 
Wire Wire Line
	1850 1050 1050 1050
Wire Wire Line
	1850 1250 1050 1250
Wire Wire Line
	1850 1450 1050 1450
Wire Wire Line
	9500 5450 9500 5650
Wire Wire Line
	10250 4250 9300 4250
Wire Wire Line
	10350 5350 9500 5350
Wire Wire Line
	9200 4350 9650 4350
Wire Wire Line
	9950 4350 10250 4350
Wire Notes Line
	8700 1800 10900 1800
Wire Wire Line
	7550 950  6900 950 
Wire Wire Line
	7550 1150 6900 1150
Wire Wire Line
	7550 1350 6900 1350
Wire Wire Line
	7550 1550 6900 1550
Wire Wire Line
	8650 1150 8050 1150
Wire Wire Line
	8650 1350 8050 1350
Wire Wire Line
	8650 1550 8050 1550
Wire Wire Line
	10600 900  10250 900 
Wire Wire Line
	10600 1100 10250 1100
Wire Wire Line
	9500 5450 9800 5450
Wire Wire Line
	10100 5450 10350 5450
Connection ~ 9650 5450
Connection ~ 10250 5450
Connection ~ 9500 4350
Wire Wire Line
	4125 4600 4125 5350
Wire Wire Line
	3450 4450 3350 4450
Wire Wire Line
	5250 2925 5250 2800
Wire Wire Line
	1250 3600 2450 3600
Wire Wire Line
	7300 5350 1300 5350
Wire Wire Line
	3375 4850 4525 4850
Connection ~ 4125 4850
Wire Wire Line
	4900 2800 4900 4075
Connection ~ 4900 3600
Wire Wire Line
	1400 4050 1400 4300
Wire Wire Line
	1400 4600 1400 4800
Wire Wire Line
	1400 4200 1550 4200
Wire Wire Line
	1550 4200 1550 4450
Connection ~ 1400 4200
Wire Wire Line
	1400 4700 1150 4700
Connection ~ 1400 4700
Wire Wire Line
	5500 3600 7950 3600
Wire Wire Line
	7300 3600 7300 3875
Connection ~ 7300 3600
Wire Wire Line
	7300 4175 7300 4275
Wire Wire Line
	7300 4575 7300 4750
Wire Wire Line
	6450 3600 6450 4775
Connection ~ 6450 3600
Wire Wire Line
	7300 4200 7550 4200
Connection ~ 7300 4200
Wire Wire Line
	7300 4625 7600 4625
Connection ~ 7300 4625
Wire Wire Line
	7300 5050 7300 5350
Wire Wire Line
	6450 5075 6450 5350
Connection ~ 6450 5350
Wire Wire Line
	7300 5150 7650 5150
Connection ~ 7300 5150
Wire Wire Line
	4900 4075 5100 4075
Wire Wire Line
	5400 4075 5650 4075
Wire Wire Line
	5650 4075 5650 3600
Connection ~ 5650 3600
Wire Wire Line
	4525 4850 4525 4550
Wire Wire Line
	4525 4100 4525 4250
Wire Notes Line
	8700 1800 8700 3850
Wire Notes Line
	10650 4100 9100 4100
Wire Notes Line
	11000 3900 8850 3900
Wire Notes Line
	8850 3900 8850 6150
Wire Wire Line
	3250 7175 4600 7175
Wire Wire Line
	3850 7175 3850 6750
Wire Wire Line
	3850 6000 3850 6150
Wire Wire Line
	2675 6000 3850 6000
Wire Wire Line
	4200 7175 4200 7350
Connection ~ 4200 7175
Wire Notes Line
	750  5650 6850 5650
Wire Notes Line
	6850 5650 6850 7750
Wire Wire Line
	9100 950  9650 950 
Wire Wire Line
	9100 1150 9650 1150
Wire Notes Line
	2450 650  550  650 
Wire Notes Line
	500  1700 2450 1700
Wire Notes Line
	6750 1750 11150 1750
Wire Notes Line
	6850 750  11050 750 
Wire Wire Line
	1850 1600 1050 1600
Wire Wire Line
	7550 4200 7550 4425
Wire Wire Line
	7550 4425 7450 4425
Wire Wire Line
	9200 4350 9200 4550
Wire Wire Line
	9500 4350 9500 5150
Wire Wire Line
	10250 5450 10250 6300
Wire Wire Line
	9650 5450 9650 6300
Wire Wire Line
	10200 1500 10550 1500
Connection ~ 10100 4350
Wire Wire Line
	10100 4350 10100 5150
Wire Wire Line
	1400 3600 1400 3750
Connection ~ 1400 3600
Connection ~ 1400 5350
Wire Wire Line
	1750 4200 2800 4200
Wire Wire Line
	2800 4200 2800 4300
Wire Wire Line
	1750 4200 1750 4300
Wire Wire Line
	1750 4600 1750 4700
Wire Wire Line
	1750 4700 2800 4700
Wire Wire Line
	2800 4700 2800 4600
Wire Wire Line
	2100 4200 2100 4300
Connection ~ 2100 4200
Wire Wire Line
	2100 4600 2100 4700
Connection ~ 2100 4700
Wire Wire Line
	2450 4300 2450 4200
Connection ~ 2450 4200
Wire Wire Line
	2450 4600 2450 4700
Connection ~ 2450 4700
Wire Wire Line
	2250 3600 2250 4200
Connection ~ 2250 4200
Connection ~ 2250 3600
Wire Wire Line
	2250 4700 2250 5350
Connection ~ 2250 5350
Connection ~ 2250 4700
Wire Wire Line
	9650 4650 9500 4650
Connection ~ 9500 4650
Wire Wire Line
	9950 4650 10100 4650
Connection ~ 10100 4650
Wire Wire Line
	9650 4900 9500 4900
Connection ~ 9500 4900
Wire Wire Line
	9950 4900 10100 4900
Connection ~ 10100 4900
Wire Wire Line
	9800 5650 9650 5650
Connection ~ 9650 5650
Wire Wire Line
	10100 5650 10250 5650
Connection ~ 10250 5650
Wire Wire Line
	9800 5850 9650 5850
Connection ~ 9650 5850
Wire Wire Line
	10100 5850 10250 5850
Connection ~ 10250 5850
Wire Wire Line
	6800 4775 6800 3600
Connection ~ 6800 3600
Wire Wire Line
	6800 5075 6800 5350
Connection ~ 6800 5350
Wire Wire Line
	5925 4775 5925 3600
Connection ~ 5925 3600
Wire Wire Line
	6150 3600 6150 4775
Connection ~ 6150 3600
Wire Wire Line
	6150 5075 6150 5350
Connection ~ 6150 5350
Wire Wire Line
	5925 5075 5925 5350
Connection ~ 5925 5350
Wire Wire Line
	3250 6600 3250 6000
Connection ~ 3250 6000
Wire Wire Line
	3250 6900 3250 7175
Connection ~ 3850 7175
Wire Wire Line
	1400 5100 1400 5350
Wire Wire Line
	2675 5525 2825 5525
Wire Wire Line
	2825 5525 2825 5350
Connection ~ 2825 5350
Wire Wire Line
	4200 7350 4125 7350
Wire Wire Line
	4525 4100 4125 4100
Connection ~ 4125 4100
Connection ~ 4125 3600
Connection ~ 4125 5350
Wire Wire Line
	4600 6450 4250 6450
Text Label 8050 975  0    60   ~ 0
VL+
Text HLabel 8650 975  2    60   Output ~ 0
VL+
Wire Wire Line
	8650 975  8050 975 
$EndSCHEMATC
