EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Power_converter_V4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 3875 2125 0    79   ~ 0
LOW-SIDE VOLTAGE SENSING CHAIN
Text Notes 3875 2325 0    59   ~ 0
Senses the voltage isolated from high power side.
Text Notes 800  650  0    60   ~ 0
Input Variables
Text Label 3150 1650 2    60   ~ 0
GNDLow
Text Label 4000 800  2    60   ~ 0
GNDPwR
Text Label 3100 1450 2    60   ~ 0
+5VLow
Text Label 3150 1850 2    60   ~ 0
+5VHigh
Text HLabel 2750 1650 0    60   Input ~ 0
GNDLow
Text HLabel 3600 800  0    60   Input ~ 0
GNDPwR
Text HLabel 2700 1450 0    60   Input ~ 0
+5VLow
Text HLabel 2750 1850 0    60   Input ~ 0
+5VHigh
Text Label 1350 1050 2    60   ~ 0
ViL-
Text HLabel 950  1050 0    60   Input ~ 0
ViL-
Text Label 1350 1450 2    60   ~ 0
ViH-
Text Label 1350 1250 2    60   ~ 0
ViH2+
Text HLabel 950  1450 0    60   Input ~ 0
ViH-
Text HLabel 950  1250 0    60   Input ~ 0
ViH+
Text Label 1300 1850 2    60   ~ 0
VL-
Text Label 1300 1650 2    60   ~ 0
VL+
Text HLabel 900  1850 0    60   Input ~ 0
VL-
Text HLabel 900  1650 0    60   Input ~ 0
VL+
Text Label 2200 1050 2    60   ~ 0
VH-
Text Label 2200 850  2    60   ~ 0
VH+
Text HLabel 1800 1050 0    60   Input ~ 0
VH-
Text HLabel 1800 850  0    60   Input ~ 0
VH+
Text Label 2200 1450 2    60   ~ 0
VT1-
Text Label 2200 1250 2    60   ~ 0
VT1+
Text HLabel 1800 1450 0    60   Input ~ 0
VT1-
Text HLabel 1800 1250 0    60   Input ~ 0
VT1+
Text Notes 11000 5900 2    60   ~ 0
Output Variables
Text Label 10250 6200 0    60   ~ 0
miso
Text HLabel 10650 6200 2    60   Output ~ 0
miso
Text Notes 650  4125 0    79   ~ 0
HIGH-SIDE CURRENT SENSING CHAIN
Text Notes 650  4275 0    59   ~ 0
Senses the current isolated from high power side.
Text Notes 650  2150 0    79   ~ 0
LOW-SIDE CURRENT SENSING CHAIN
Text Notes 650  2300 0    59   ~ 0
Senses the current isolated from high power side.
Text Notes 3975 4175 0    79   ~ 0
HIGH-SIDE VOLTAGE SENSING CHAIN
Text Notes 4025 4325 0    59   ~ 0
Senses the voltage isolated from high power side.
Text Notes 550  6225 0    79   ~ 0
TRANSISTOR TEMPERATURE SENSING CHAIN
Text Notes 600  6425 0    59   ~ 0
Senses the voltage issued from the temperature sensor.
Text Notes 7125 975  0    79   ~ 0
A/D CONVERTER AND DIGITAL ISOLATOR UNIT
Text Notes 7375 1350 0    59   ~ 0
This part 
$Comp
L MCP3208-RESCUE-Power_converter_V4 UADC1
U 1 1 5975F2C8
P 7275 2275
F 0 "UADC1" H 7025 2775 50  0000 R CNN
F 1 "MCP3208" H 7925 1675 50  0000 R CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 7375 2375 50  0001 C CNN
F 3 "" H 7375 2375 50  0001 C CNN
	1    7275 2275
	1    0    0    -1  
$EndComp
$Comp
L ISO7341C Digitiso1
U 1 1 5975F2CF
P 9125 2275
F 0 "Digitiso1" H 8875 2875 50  0000 C CNN
F 1 "ISO7341C" H 8825 1675 50  0000 C CNN
F 2 "SMD_Packages:SO-16-W" H 8225 1675 50  0001 C CIN
F 3 "" H 9125 2675 50  0000 C CNN
	1    9125 2275
	-1   0    0    -1  
$EndComp
Text Label 10350 1975 0    60   ~ 0
GNDLow
Text Label 6925 3175 2    60   ~ 0
GNDPwR
Text Label 10300 1425 0    60   ~ 0
+5VLow
Text Label 6725 1425 2    60   ~ 0
+5VHigh
Text Label 8150 1975 2    60   ~ 0
GNDPwR
NoConn ~ 6675 2675
Text Label 6375 1975 0    48   ~ 0
VmiL+
Text Label 6375 2075 0    48   ~ 0
VmiH+
Text Label 6375 2175 0    48   ~ 0
VmL+
Text Label 6375 2275 0    48   ~ 0
VmH+
Text Label 6375 2375 0    48   ~ 0
VmT1+
Text Label 9925 2375 2    60   ~ 0
clk
Text Label 9925 2475 2    60   ~ 0
mosi
Text Label 9925 2575 2    60   ~ 0
ss
Text Label 9925 2675 2    60   ~ 0
miso
Text Label 3525 3150 0    60   ~ 0
VmiL+
Text Label 2475 2600 2    60   ~ 0
+5VHigh
$Comp
L OP275 U2
U 1 1 5975FBC0
P 2775 3150
F 0 "U2" H 2775 3300 50  0000 L CNN
F 1 "OPA2336" H 2775 3000 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 2675 3200 50  0001 C CNN
F 3 "" H 2775 3300 50  0000 C CNN
F 4 "Value" H 2775 3150 60  0000 C CNN "Bibliothèque/Application support/kicad/library/shield_arduino_Schematic"
F 5 "Value" H 2775 3150 60  0000 C CNN "Bibliothèque/Application support/kicad/library/SHIELD_ARDUINO_empreintes"
F 6 "Value" H 2775 3150 60  0000 C CNN "Bibliothèque/Application support/kicad/library/Shield_Arduino_3D"
F 7 "Value" H 2775 3150 60  0001 C CNN "Fieldname"
	1    2775 3150
	1    0    0    -1  
$EndComp
$Comp
L R Rp1
U 1 1 5975FBC8
P 2225 3450
F 0 "Rp1" V 2305 3450 50  0000 C CNN
F 1 "R" V 2225 3450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 2155 3450 50  0001 C CNN
F 3 "" H 2225 3450 50  0000 C CNN
	1    2225 3450
	1    0    0    -1  
$EndComp
$Comp
L C CfiL1
U 1 1 5975FBE1
P 2025 3350
F 0 "CfiL1" V 2075 3400 50  0000 L CNN
F 1 "1uF" V 2075 3150 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2063 3200 50  0001 C CNN
F 3 "" H 2025 3350 50  0000 C CNN
	1    2025 3350
	-1   0    0    1   
$EndComp
Text Label 800  3150 2    60   ~ 0
ViL-
Text Label 2675 3750 2    60   ~ 0
GNDPwR
Text Label 3525 5050 0    60   ~ 0
VmiH+
Text Label 2475 4500 2    60   ~ 0
+5VHigh
$Comp
L OP275 U3
U 1 1 597603B0
P 2775 5050
F 0 "U3" H 2775 5200 50  0000 L CNN
F 1 "OPA2336" H 2775 4900 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 2675 5100 50  0001 C CNN
F 3 "" H 2775 5200 50  0000 C CNN
F 4 "Value" H 2775 5050 60  0000 C CNN "Bibliothèque/Application support/kicad/library/shield_arduino_Schematic"
F 5 "Value" H 2775 5050 60  0000 C CNN "Bibliothèque/Application support/kicad/library/SHIELD_ARDUINO_empreintes"
F 6 "Value" H 2775 5050 60  0000 C CNN "Bibliothèque/Application support/kicad/library/Shield_Arduino_3D"
F 7 "Value" H 2775 5050 60  0001 C CNN "Fieldname"
	1    2775 5050
	1    0    0    -1  
$EndComp
Text Label 825  4850 2    60   ~ 0
ViH2+
$Comp
L R Rp2
U 1 1 597603B8
P 2225 5350
F 0 "Rp2" V 2305 5350 50  0000 C CNN
F 1 "R" V 2225 5350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 2155 5350 50  0001 C CNN
F 3 "" H 2225 5350 50  0000 C CNN
	1    2225 5350
	1    0    0    -1  
$EndComp
$Comp
L C CfiH1
U 1 1 597603D1
P 2025 5250
F 0 "CfiH1" V 2075 5300 50  0000 L CNN
F 1 "1uF" V 2075 5050 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2063 5100 50  0001 C CNN
F 3 "" H 2025 5250 50  0000 C CNN
	1    2025 5250
	-1   0    0    1   
$EndComp
$Comp
L R RfiH1
U 1 1 597603D8
P 1825 4950
F 0 "RfiH1" V 1905 4950 50  0000 C CNN
F 1 "100" V 1825 4950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1755 4950 50  0001 C CNN
F 3 "" H 1825 4950 50  0000 C CNN
	1    1825 4950
	0    -1   -1   0   
$EndComp
Text Label 825  5050 2    60   ~ 0
ViH-
Text Label 2675 5600 2    60   ~ 0
GNDPwR
Text Label 5875 3200 0    60   ~ 0
VmL+
Text Label 5025 2650 2    60   ~ 0
+5VHigh
$Comp
L OP275 U2
U 2 1 597606D0
P 5125 3200
F 0 "U2" H 5125 3350 50  0000 L CNN
F 1 "OPA2336" H 5125 3050 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5025 3250 50  0001 C CNN
F 3 "" H 5125 3350 50  0000 C CNN
F 4 "Value" H 5125 3200 60  0000 C CNN "Bibliothèque/Application support/kicad/library/shield_arduino_Schematic"
F 5 "Value" H 5125 3200 60  0000 C CNN "Bibliothèque/Application support/kicad/library/SHIELD_ARDUINO_empreintes"
F 6 "Value" H 5125 3200 60  0000 C CNN "Bibliothèque/Application support/kicad/library/Shield_Arduino_3D"
F 7 "Value" H 5125 3200 60  0001 C CNN "Fieldname"
	2    5125 3200
	1    0    0    -1  
$EndComp
Text Label 4475 3100 2    60   ~ 0
VL+
Text Label 6050 5125 0    60   ~ 0
VmH+
$Comp
L OP275 U3
U 2 1 59760A60
P 5300 5125
F 0 "U3" H 5300 5275 50  0000 L CNN
F 1 "OPA2336" H 5300 4975 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5200 5175 50  0001 C CNN
F 3 "" H 5300 5275 50  0000 C CNN
F 4 "Value" H 5300 5125 60  0000 C CNN "Bibliothèque/Application support/kicad/library/shield_arduino_Schematic"
F 5 "Value" H 5300 5125 60  0000 C CNN "Bibliothèque/Application support/kicad/library/SHIELD_ARDUINO_empreintes"
F 6 "Value" H 5300 5125 60  0000 C CNN "Bibliothèque/Application support/kicad/library/Shield_Arduino_3D"
F 7 "Value" H 5300 5125 60  0001 C CNN "Fieldname"
	2    5300 5125
	1    0    0    -1  
$EndComp
Text Label 4450 5025 2    60   ~ 0
VH+
Text Label 2625 7225 0    60   ~ 0
VmT1+
Text Label 1775 6725 2    60   ~ 0
+5VHigh
$Comp
L OP275 U1
U 1 1 59760E9B
P 1875 7225
F 0 "U1" H 1875 7375 50  0000 L CNN
F 1 "OPA2336" H 1875 7075 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 1775 7275 50  0001 C CNN
F 3 "" H 1875 7375 50  0000 C CNN
F 4 "Value" H 1875 7225 60  0000 C CNN "Bibliothèque/Application support/kicad/library/shield_arduino_Schematic"
F 5 "Value" H 1875 7225 60  0000 C CNN "Bibliothèque/Application support/kicad/library/SHIELD_ARDUINO_empreintes"
F 6 "Value" H 1875 7225 60  0000 C CNN "Bibliothèque/Application support/kicad/library/Shield_Arduino_3D"
F 7 "Value" H 1875 7225 60  0001 C CNN "Fieldname"
	1    1875 7225
	1    0    0    -1  
$EndComp
Text Label 1150 7125 2    60   ~ 0
VT1+
Text Label 4475 3425 2    60   ~ 0
VL-
Text Label 4475 5325 2    60   ~ 0
VH-
Text Label 1150 7425 2    60   ~ 0
VT1-
Text Label 2950 850  2    60   ~ 0
clk
Text HLabel 2550 850  0    60   Input ~ 0
clk
Text Label 2950 1050 2    60   ~ 0
ss
Text HLabel 2550 1050 0    60   Input ~ 0
ss
Text Label 3050 1250 2    60   ~ 0
mosi
Text HLabel 2650 1250 0    60   Input ~ 0
mosi
Text Label 4000 1000 2    60   ~ 0
Pgood
Text HLabel 3600 1000 0    60   Input ~ 0
Pgood
Text Label 6375 2475 0    60   ~ 0
Pgood
Text Label 1350 7625 2    60   ~ 0
GNDPwR
Text Label 4675 3625 2    60   ~ 0
GNDPwR
Text Label 4675 5650 2    60   ~ 0
GNDPwR
Text Label 1350 900  2    60   ~ 0
ViL2+
Text HLabel 950  900  0    60   Input ~ 0
ViL+
Text Label 800  2950 2    60   ~ 0
ViL2+
$Comp
L R RfiL1
U 1 1 5975FBE8
P 1825 3050
F 0 "RfiL1" V 1905 3050 50  0000 C CNN
F 1 "100" V 1825 3050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1755 3050 50  0001 C CNN
F 3 "" H 1825 3050 50  0000 C CNN
	1    1825 3050
	0    1    1    0   
$EndComp
$Comp
L CONN_01X01 TH6
U 1 1 599F4D1D
P 10350 2900
F 0 "TH6" H 10350 3000 50  0000 C CNN
F 1 "CONN_01X01" V 10450 2900 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 10350 2900 50  0001 C CNN
F 3 "" H 10350 2900 50  0000 C CNN
	1    10350 2900
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TH7
U 1 1 599F4E13
P 10350 3150
F 0 "TH7" H 10350 3250 50  0000 C CNN
F 1 "CONN_01X01" V 10450 3150 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 10350 3150 50  0001 C CNN
F 3 "" H 10350 3150 50  0000 C CNN
	1    10350 3150
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TH8
U 1 1 599F4E96
P 10350 3400
F 0 "TH8" H 10350 3500 50  0000 C CNN
F 1 "CONN_01X01" V 10450 3400 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 10350 3400 50  0001 C CNN
F 3 "" H 10350 3400 50  0000 C CNN
	1    10350 3400
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TH9
U 1 1 599F4F13
P 10350 3650
F 0 "TH9" H 10350 3750 50  0000 C CNN
F 1 "CONN_01X01" V 10450 3650 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 10350 3650 50  0001 C CNN
F 3 "" H 10350 3650 50  0000 C CNN
	1    10350 3650
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TH10
U 1 1 599F4F9A
P 10350 3900
F 0 "TH10" H 10350 4000 50  0000 C CNN
F 1 "CONN_01X01" V 10450 3900 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 10350 3900 50  0001 C CNN
F 3 "" H 10350 3900 50  0000 C CNN
	1    10350 3900
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TH11
U 1 1 599F5C5B
P 10350 4150
F 0 "TH11" H 10350 4250 50  0000 C CNN
F 1 "CONN_01X01" V 10450 4150 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 10350 4150 50  0001 C CNN
F 3 "" H 10350 4150 50  0000 C CNN
	1    10350 4150
	-1   0    0    1   
$EndComp
Text Label 10750 2900 0    48   ~ 0
VmiL+
Text Label 10750 3150 0    48   ~ 0
VmiH+
Text Label 10750 3400 0    48   ~ 0
VmL+
Text Label 10750 3650 0    48   ~ 0
VmH+
Text Label 10750 3900 0    48   ~ 0
VmT1+
Text Label 10750 4150 0    60   ~ 0
Pgood
Wire Wire Line
	1975 3050 2025 3050
Wire Wire Line
	2025 3050 2475 3050
Wire Wire Line
	2025 3200 2025 3050
Wire Notes Line
	3825 2225 6125 2225
Wire Wire Line
	2750 1650 3150 1650
Wire Wire Line
	3600 800  4000 800 
Wire Wire Line
	2700 1450 3100 1450
Wire Wire Line
	2750 1850 3150 1850
Wire Wire Line
	950  1050 1350 1050
Wire Wire Line
	950  1450 1350 1450
Wire Wire Line
	950  1250 1350 1250
Wire Wire Line
	900  1850 1300 1850
Wire Wire Line
	900  1650 1300 1650
Wire Wire Line
	1800 1050 2200 1050
Wire Wire Line
	1800 850  2200 850 
Wire Wire Line
	1800 1450 2200 1450
Wire Wire Line
	1800 1250 2200 1250
Wire Wire Line
	10650 6200 10250 6200
Wire Notes Line
	4050 1950 4050 650 
Wire Notes Line
	600  4175 2900 4175
Wire Notes Line
	600  2200 2900 2200
Wire Notes Line
	3975 4225 6275 4225
Wire Notes Line
	750  6325 3050 6325
Wire Notes Line
	700  700  1500 700 
Wire Notes Line
	10250 5950 11050 5950
Wire Notes Line
	6925 1075 9225 1075
Wire Wire Line
	8375 2375 8725 2375
Wire Wire Line
	8275 2675 8725 2675
Wire Wire Line
	8175 2475 8725 2475
Wire Wire Line
	8175 2375 8175 2475
Wire Wire Line
	7875 2375 8175 2375
Wire Wire Line
	8725 2575 8075 2575
Wire Wire Line
	8075 2575 8075 2475
Wire Wire Line
	8075 2475 7875 2475
Wire Wire Line
	7875 2275 8275 2275
Wire Wire Line
	8275 2275 8275 2675
Wire Wire Line
	6725 1425 7175 1425
Wire Wire Line
	7175 1425 7475 1425
Wire Wire Line
	7475 1425 8475 1425
Wire Wire Line
	7475 1425 7475 1625
Wire Wire Line
	7475 1625 7475 1775
Connection ~ 7475 1425
Wire Wire Line
	7175 1775 7175 1425
Connection ~ 7175 1425
Wire Wire Line
	7175 2875 7175 3075
Wire Wire Line
	7175 3075 7325 3075
Wire Wire Line
	7325 3075 7475 3075
Wire Wire Line
	7475 3075 7475 2875
Wire Wire Line
	7325 3075 7325 3175
Connection ~ 7325 3075
Wire Wire Line
	9525 1875 10000 1875
Wire Wire Line
	10000 1875 10200 1875
Wire Wire Line
	10000 1425 10300 1425
Connection ~ 10000 1875
Wire Wire Line
	9525 1975 9700 1975
Wire Wire Line
	9700 1975 10350 1975
Wire Wire Line
	9525 2075 9700 2075
Wire Wire Line
	9700 2075 9700 1975
Connection ~ 9700 1975
Wire Wire Line
	8150 1975 8175 1975
Wire Wire Line
	8175 1975 8275 1975
Wire Wire Line
	8275 1975 8575 1975
Wire Wire Line
	8575 1975 8725 1975
Wire Wire Line
	8725 2075 8575 2075
Wire Wire Line
	8575 2075 8575 1975
Connection ~ 8575 1975
Wire Wire Line
	8475 1875 8725 1875
Wire Wire Line
	8375 2175 8375 2375
Wire Wire Line
	7875 2175 8375 2175
Connection ~ 8475 1875
Wire Wire Line
	9525 2375 9925 2375
Wire Wire Line
	9525 2475 9925 2475
Wire Wire Line
	9525 2575 9925 2575
Wire Wire Line
	9525 2675 9925 2675
Wire Wire Line
	6375 1975 6675 1975
Wire Wire Line
	6375 2075 6675 2075
Wire Wire Line
	6375 2175 6675 2175
Wire Wire Line
	6375 2275 6675 2275
Wire Wire Line
	6375 2375 6675 2375
Wire Wire Line
	3075 3150 3275 3150
Wire Wire Line
	3275 3150 3525 3150
Wire Wire Line
	2675 2600 2675 2850
Wire Wire Line
	2675 3450 2675 3750
Wire Wire Line
	2225 3250 2425 3250
Wire Wire Line
	2425 3250 2475 3250
Wire Wire Line
	2225 3250 2225 3300
Wire Wire Line
	2675 2600 2475 2600
Wire Wire Line
	2425 3250 2425 3500
Wire Wire Line
	2425 3500 2725 3500
Connection ~ 2425 3250
Wire Wire Line
	3025 3500 3150 3500
Wire Wire Line
	3150 3500 3275 3500
Wire Wire Line
	3275 3500 3275 3150
Connection ~ 3275 3150
Wire Wire Line
	2225 3675 2225 3600
Connection ~ 2025 3050
Wire Wire Line
	2025 3500 2025 3675
Wire Wire Line
	1550 3675 2025 3675
Wire Wire Line
	2025 3675 2225 3675
Connection ~ 2025 3675
Wire Wire Line
	3075 5050 3275 5050
Wire Wire Line
	3275 5050 3525 5050
Wire Wire Line
	2675 4500 2675 4625
Wire Wire Line
	2675 4625 2675 4750
Wire Wire Line
	2675 5350 2675 5600
Wire Wire Line
	2225 5150 2425 5150
Wire Wire Line
	2425 5150 2475 5150
Wire Wire Line
	2225 5150 2225 5200
Wire Wire Line
	1975 4950 2025 4950
Wire Wire Line
	2025 4950 2475 4950
Wire Wire Line
	2675 4500 2475 4500
Wire Wire Line
	2425 5150 2425 5400
Wire Wire Line
	2425 5400 2725 5400
Connection ~ 2425 5150
Wire Wire Line
	3025 5400 3150 5400
Wire Wire Line
	3150 5400 3275 5400
Wire Wire Line
	3275 5400 3275 5050
Connection ~ 3275 5050
Wire Wire Line
	2225 5600 2225 5500
Wire Wire Line
	2025 4950 2025 5100
Connection ~ 2025 4950
Wire Wire Line
	2025 5400 2025 5600
Wire Wire Line
	1550 5600 2025 5600
Wire Wire Line
	2025 5600 2225 5600
Connection ~ 2025 5600
Wire Wire Line
	5425 3200 5575 3200
Wire Wire Line
	5575 3200 5875 3200
Wire Wire Line
	5025 2650 5025 2725
Wire Wire Line
	5025 2725 5025 2900
Wire Wire Line
	4825 3300 4725 3300
Wire Wire Line
	4725 3300 4725 3550
Wire Wire Line
	4725 3550 5575 3550
Wire Wire Line
	5575 3550 5575 3200
Connection ~ 5575 3200
Wire Wire Line
	4475 3100 4825 3100
Wire Wire Line
	5600 5125 5750 5125
Wire Wire Line
	5750 5125 6050 5125
Wire Wire Line
	5000 5225 4900 5225
Wire Wire Line
	4900 5225 4900 5525
Wire Wire Line
	4900 5525 5750 5525
Wire Wire Line
	5750 5525 5750 5125
Connection ~ 5750 5125
Wire Wire Line
	4450 5025 5000 5025
Wire Wire Line
	2175 7225 2325 7225
Wire Wire Line
	2325 7225 2625 7225
Wire Wire Line
	1775 6725 1775 6775
Wire Wire Line
	1775 6775 1775 6925
Wire Wire Line
	1775 7675 1775 7525
Wire Wire Line
	1575 7325 1475 7325
Wire Wire Line
	1475 7325 1475 7575
Wire Wire Line
	1475 7575 2325 7575
Wire Wire Line
	2325 7575 2325 7225
Connection ~ 2325 7225
Wire Wire Line
	1150 7125 1575 7125
Wire Wire Line
	1350 7675 1775 7675
Wire Notes Line
	500  1950 4050 1950
Wire Wire Line
	4475 3425 4675 3425
Wire Wire Line
	4675 3425 4675 3650
Wire Wire Line
	4475 5325 4675 5325
Wire Wire Line
	4675 5325 4675 5650
Wire Wire Line
	1150 7425 1350 7425
Wire Wire Line
	1350 7425 1350 7675
Wire Notes Line
	9075 1425 9075 3025
Wire Notes Line
	9175 1425 9175 3025
Wire Wire Line
	7325 3175 6925 3175
Wire Notes Line
	11200 5600 10000 5600
Wire Notes Line
	10000 5600 10000 6500
Wire Notes Line
	10000 6500 11150 6500
Wire Wire Line
	2550 850  2950 850 
Wire Wire Line
	2550 1050 2950 1050
Wire Wire Line
	2650 1250 3050 1250
Wire Wire Line
	3600 1000 4000 1000
Wire Wire Line
	6675 2475 6375 2475
Wire Wire Line
	950  900  1350 900 
Wire Wire Line
	10550 2900 10750 2900
Wire Wire Line
	10550 3150 10750 3150
Wire Wire Line
	10550 3400 10750 3400
Wire Wire Line
	10550 3650 10750 3650
Wire Wire Line
	10550 3900 10750 3900
Wire Wire Line
	10550 4150 10750 4150
Wire Wire Line
	1675 4950 1550 4950
Wire Wire Line
	1550 3050 1675 3050
$Comp
L R_Small RisoL1
U 1 1 59B28A0D
P 10050 2175
F 0 "RisoL1" H 10080 2195 50  0000 L CNN
F 1 "4.7k" H 10080 2135 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 10050 2175 50  0001 C CNN
F 3 "" H 10050 2175 50  0001 C CNN
	1    10050 2175
	0    1    1    0   
$EndComp
Wire Wire Line
	10200 1875 10200 2175
Wire Wire Line
	10000 1425 10000 1625
Wire Wire Line
	10000 1625 10000 1875
Wire Wire Line
	9525 2175 9950 2175
Wire Wire Line
	10200 2175 10150 2175
$Comp
L R_Small RisoH1
U 1 1 59B294DE
P 8475 2100
F 0 "RisoH1" H 8505 2120 50  0000 L CNN
F 1 "4.7k" H 8505 2060 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 8475 2100 50  0001 C CNN
F 3 "" H 8475 2100 50  0001 C CNN
	1    8475 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8725 2175 8725 2250
Wire Wire Line
	8725 2250 8475 2250
Wire Wire Line
	8475 2250 8475 2200
Wire Wire Line
	8475 1425 8475 1625
Wire Wire Line
	8475 1625 8475 1875
Wire Wire Line
	8475 1875 8475 2000
$Comp
L C_Small CdisoH1
U 1 1 59B29B19
P 8275 1800
F 0 "CdisoH1" H 8285 1870 50  0000 L CNN
F 1 "100n" H 8285 1720 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 8275 1800 50  0001 C CNN
F 3 "" H 8275 1800 50  0001 C CNN
	1    8275 1800
	1    0    0    -1  
$EndComp
$Comp
L C_Small CdisoL1
U 1 1 59B29BA0
P 10350 1775
F 0 "CdisoL1" H 10360 1845 50  0000 L CNN
F 1 "100n" H 10360 1695 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 10350 1775 50  0001 C CNN
F 3 "" H 10350 1775 50  0001 C CNN
	1    10350 1775
	1    0    0    -1  
$EndComp
Wire Wire Line
	8275 1900 8275 1975
Connection ~ 8275 1975
Wire Wire Line
	8275 1700 8275 1625
Wire Wire Line
	8275 1625 8475 1625
Connection ~ 8475 1625
Wire Wire Line
	10350 1675 10350 1625
Wire Wire Line
	10350 1625 10000 1625
Connection ~ 10000 1625
$Comp
L C_Small Cdadc1
U 1 1 59B2AF71
P 7675 1625
F 0 "Cdadc1" H 7685 1695 50  0000 L CNN
F 1 "100n" H 7685 1545 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 7675 1625 50  0001 C CNN
F 3 "" H 7675 1625 50  0001 C CNN
	1    7675 1625
	0    1    1    0   
$EndComp
Wire Wire Line
	7775 1625 8175 1625
Wire Wire Line
	8175 1625 8175 1975
Connection ~ 8175 1975
Wire Wire Line
	7575 1625 7475 1625
Connection ~ 7475 1625
Text Label 3375 4525 2    60   ~ 0
GNDPwR
$Comp
L C_Small Cdopamp2
U 1 1 59B2B545
P 2875 4625
F 0 "Cdopamp2" H 2885 4695 50  0000 L CNN
F 1 "100n" H 2885 4545 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2875 4625 50  0001 C CNN
F 3 "" H 2875 4625 50  0001 C CNN
	1    2875 4625
	0    1    1    0   
$EndComp
Wire Wire Line
	2775 4625 2675 4625
Connection ~ 2675 4625
Wire Wire Line
	2975 4625 3375 4625
Wire Wire Line
	3375 4625 3375 4525
$Comp
L C_Small Cdopamp3
U 1 1 59B2BCAC
P 5300 2725
F 0 "Cdopamp3" H 5310 2795 50  0000 L CNN
F 1 "100n" H 5310 2645 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 5300 2725 50  0001 C CNN
F 3 "" H 5300 2725 50  0001 C CNN
	1    5300 2725
	0    1    1    0   
$EndComp
$Comp
L C_Small Cdopamp1
U 1 1 59B2BE95
P 2000 6775
F 0 "Cdopamp1" H 2010 6845 50  0000 L CNN
F 1 "100n" H 2010 6695 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 2000 6775 50  0001 C CNN
F 3 "" H 2000 6775 50  0001 C CNN
	1    2000 6775
	0    1    1    0   
$EndComp
Text Label 5575 2725 0    60   ~ 0
GNDPwR
Text Label 2300 6775 0    60   ~ 0
GNDPwR
Wire Wire Line
	1900 6775 1775 6775
Connection ~ 1775 6775
Wire Wire Line
	2100 6775 2300 6775
Wire Wire Line
	5200 2725 5025 2725
Connection ~ 5025 2725
Wire Wire Line
	5400 2725 5575 2725
Wire Wire Line
	10350 1975 10350 1875
Connection ~ 10350 1975
$Comp
L Quad_shottky_x2 U4
U 1 1 59DC1812
P 7925 5225
F 0 "U4" H 8050 5225 60  0000 C CNN
F 1 "Quad_shottky_x2" H 7850 5400 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-363_SC-70-6_Handsoldering" H 7975 5225 60  0001 C CNN
F 3 "" H 7975 5225 60  0001 C CNN
	1    7925 5225
	1    0    0    -1  
$EndComp
Text Label 7875 4500 2    60   ~ 0
+5VHigh
Text Label 7875 5625 2    60   ~ 0
GNDPwR
Wire Wire Line
	7875 5475 7875 5625
Wire Wire Line
	7875 4500 7875 4675
Wire Wire Line
	5025 3650 5025 3500
Wire Wire Line
	4675 3650 5025 3650
Text Label 5125 4700 2    60   ~ 0
+5VHigh
Wire Wire Line
	5125 4700 5200 4700
Wire Wire Line
	5200 4700 5200 4825
Wire Wire Line
	4675 5650 5200 5650
Wire Wire Line
	5200 5650 5200 5425
Text Label 7350 5625 2    60   ~ 0
VL+
Text Label 8225 5625 2    60   ~ 0
VT1+
Wire Wire Line
	7350 4500 7475 4500
Wire Wire Line
	7475 4500 7475 4675
Wire Wire Line
	7350 5625 7475 5625
Wire Wire Line
	7475 5625 7475 5475
Wire Wire Line
	8225 5625 8225 5475
$Comp
L POT_TRIM RV1
U 1 1 59DCD419
P 2875 3500
F 0 "RV1" V 2775 3500 50  0000 C CNN
F 1 "10k" V 2875 3500 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Bourns_3296Y" H 2875 3500 50  0001 C CNN
F 3 "" H 2875 3500 50  0001 C CNN
	1    2875 3500
	0    1    1    0   
$EndComp
NoConn ~ 8225 4675
NoConn ~ 6675 2575
Text Label 7350 4500 2    60   ~ 0
VH+
Wire Wire Line
	2875 3650 3150 3650
Wire Wire Line
	3150 3650 3150 3500
Connection ~ 3150 3500
$Comp
L POT_TRIM RV2
U 1 1 59DC93E1
P 2875 5400
F 0 "RV2" V 2775 5400 50  0000 C CNN
F 1 "10k" V 2875 5400 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer_Bourns_3296Y" H 2875 5400 50  0001 C CNN
F 3 "" H 2875 5400 50  0001 C CNN
	1    2875 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	2875 5550 2875 5575
Wire Wire Line
	2875 5575 3150 5575
Wire Wire Line
	3150 5575 3150 5400
Connection ~ 3150 5400
$Comp
L SW_SPDT SW1
U 1 1 59DCF667
P 1350 3050
F 0 "SW1" H 1350 3220 50  0000 C CNN
F 1 "SW_SPDT" H 1350 2850 50  0000 C CNN
F 2 "test:switch" H 1350 3050 50  0001 C CNN
F 3 "" H 1350 3050 50  0001 C CNN
	1    1350 3050
	-1   0    0    1   
$EndComp
$Comp
L SW_SPDT SW2
U 1 1 59DCF7DE
P 1350 3675
F 0 "SW2" H 1350 3845 50  0000 C CNN
F 1 "SW_SPDT" H 1350 3475 50  0000 C CNN
F 2 "test:switch" H 1350 3675 50  0001 C CNN
F 3 "" H 1350 3675 50  0001 C CNN
	1    1350 3675
	-1   0    0    1   
$EndComp
Wire Wire Line
	800  2950 875  2950
Wire Wire Line
	875  2950 1150 2950
Wire Wire Line
	875  2950 875  3575
Wire Wire Line
	875  3575 1150 3575
Connection ~ 875  2950
Wire Wire Line
	800  3150 925  3150
Wire Wire Line
	925  3150 1150 3150
Wire Wire Line
	925  3150 925  3775
Wire Wire Line
	925  3775 1150 3775
Connection ~ 925  3150
$Comp
L SW_SPDT SW3
U 1 1 59DD15B6
P 1350 4950
F 0 "SW3" H 1350 5120 50  0000 C CNN
F 1 "SW_SPDT" H 1350 4750 50  0000 C CNN
F 2 "test:switch" H 1350 4950 50  0001 C CNN
F 3 "" H 1350 4950 50  0001 C CNN
	1    1350 4950
	-1   0    0    1   
$EndComp
$Comp
L SW_SPDT SW4
U 1 1 59DD1898
P 1350 5600
F 0 "SW4" H 1350 5770 50  0000 C CNN
F 1 "SW_SPDT" H 1350 5400 50  0000 C CNN
F 2 "test:switch" H 1350 5600 50  0001 C CNN
F 3 "" H 1350 5600 50  0001 C CNN
	1    1350 5600
	-1   0    0    1   
$EndComp
Wire Wire Line
	825  4850 950  4850
Wire Wire Line
	950  4850 1150 4850
Wire Wire Line
	950  4850 950  5500
Wire Wire Line
	950  5500 1150 5500
Connection ~ 950  4850
Wire Wire Line
	825  5050 1000 5050
Wire Wire Line
	1000 5050 1150 5050
Wire Wire Line
	1000 5050 1000 5700
Wire Wire Line
	1000 5700 1150 5700
Connection ~ 1000 5050
$EndSCHEMATC
