EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Power_converter_V4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date "20 apr 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 1000 1050 0    60   Input ~ 0
PWMH1
Text HLabel 1000 1300 0    60   Input ~ 0
PWML1
Text Notes 900  650  0    60   ~ 0
BLOCK INPUTS
Text Notes 900  800  0    60   ~ 0
----------------------------
Text Notes 8300 700  0    60   ~ 0
BLOCK OUTPUTS
Text Notes 8300 850  0    60   ~ 0
----------------------------
Text HLabel 9200 1000 2    60   Output ~ 0
VgH
$Comp
L IRS2186 DR1
U 1 1 5717CC5F
P 9100 4700
F 0 "DR1" H 9350 5400 60  0000 C CNN
F 1 "IRS2186" H 9100 4150 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-14_3.9x8.7mm_Pitch1.27mm" H 9100 4100 60  0001 C CNN
F 3 "" H 9100 4100 60  0000 C CNN
	1    9100 4700
	1    0    0    -1  
$EndComp
Text Label 2425 4050 0    60   ~ 0
PWMH
Text Label 2425 4550 0    60   ~ 0
PWML
NoConn ~ 8550 4600
NoConn ~ 9650 4750
NoConn ~ 9650 4900
NoConn ~ 9650 5050
NoConn ~ 9650 4150
Text Label 10450 4450 0    60   ~ 0
VgH
Text Label 10500 5550 0    60   ~ 0
VgL
Text HLabel 9200 1200 2    60   Output ~ 0
VsH
Text HLabel 9200 1400 2    60   Output ~ 0
VgL
Text HLabel 9200 1600 2    60   Output ~ 0
VsL
Text Label 10450 4600 0    60   ~ 0
VsH
Text Label 10500 5750 0    60   ~ 0
VsL
Text Label 1400 1050 0    60   ~ 0
PWMH
Text Label 1400 1300 0    60   ~ 0
PWML
Text HLabel 1000 1550 0    60   Input ~ 0
GNDPWM
Text Label 1400 1550 0    60   ~ 0
GNDPWM
Text Label 1550 5550 0    60   ~ 0
GNDPWM
Text Label 8400 1000 2    60   ~ 0
VgH
Text Label 8400 1400 2    60   ~ 0
VgL
Text Label 8400 1200 2    60   ~ 0
VsH
Text Label 8400 1600 2    60   ~ 0
VsL
Text Notes 2700 3200 0    60   ~ 0
ADAPTABLE JUMPERS
Text Notes 2200 3250 0    60   ~ 0
----------------------------
Text Notes 2450 3500 0    60   ~ 0
These jumpers allows the user \nto choose between a 5V or a 3.3V\ninput. 
Text Notes 4800 6300 0    60   ~ 0
OPTOCOUPLER
Text Notes 4250 6450 0    60   ~ 0
----------------------------
Text Notes 4450 6800 0    60   ~ 0
The optocoupler chosen for this \nconverter is a TLP2631 which\nhas a VCC of +5V and can go up \nto 200kHz for low power applications
Text Notes 5250 6050 1    60   ~ 0
------------------------------------------\n\n\n------------------------------------------
Text Notes 4200 5750 0    60   ~ 0
           <-\nLOW        \nVOLTAGE\nSIDE 
Text Notes 5300 5750 0    60   ~ 0
->           \n     HIGH\n     VOLTAGE\n     SIDE 
Text Notes 2850 6100 0    60   ~ 0
----------------------------
Text Notes 5200 6100 0    60   ~ 0
----------------------------
Text Notes 2850 3000 0    60   ~ 0
----------------------------
Text Notes 5250 3000 0    60   ~ 0
----------------------------
Text Notes 8600 2500 0    60   ~ 0
POWER DRIVER
Text Notes 8150 2550 0    60   ~ 0
----------------------------
Text Notes 8450 3350 0    60   ~ 0
The power driver chosen\nfor this converter is the \nIR2186 capable of driving\na transitor up to 4A at a time.\nThis solution comprises a \nbootstrap which allows driving \nboth the high and low side \ntransistors from the same IC.
Text Label 5650 3600 0    60   ~ 0
+5VHigh
Text Label 7650 5350 2    60   ~ 0
+15VHigh
Text HLabel 2350 1400 0    60   Input ~ 0
GNDPwR
Text Label 7050 3350 2    60   ~ 0
Hin
Text Label 6800 4975 2    60   ~ 0
Lin
Text HLabel 9200 1800 2    60   Output ~ 0
Hin
Text HLabel 9200 2000 2    60   Output ~ 0
Lin
Text Label 8400 1800 2    60   ~ 0
Hin
Text Label 8400 2000 2    60   ~ 0
Lin
Text Notes 5150 3850 1    60   ~ 0
WARNING: \nMASSES MUST BE SEPARATED!!!
Text HLabel 2350 1000 0    60   Input ~ 0
+5VHigh
Text HLabel 2350 1200 0    60   Input ~ 0
+15VHigh
Text Label 2750 1200 0    60   ~ 0
+15VHigh
Text Label 2750 1000 0    60   ~ 0
+5VHigh
Text Notes 3800 1300 0    60   ~ 0
This circuit connects the microcontroller \nto the power transistors. \nIt uses an optocoupler to separate the grounds\nfrom both sides.
Text Notes 3800 750  0    60   ~ 0
DETAILS OF THE DRIVER BLOCK
Text Notes 3800 850  0    60   ~ 0
-----------------------------------------------
Text Notes 6150 1200 0    60   ~ 0
Based on these components\nthe system can go up to \n150kHz to 200kHz
$Comp
L D DD1
U 1 1 5722198A
P 9050 3750
F 0 "DD1" H 9050 3850 50  0000 C CNN
F 1 "DIODE" H 9050 3650 50  0000 C CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 9050 3750 50  0001 C CNN
F 3 "" H 9050 3750 50  0000 C CNN
	1    9050 3750
	-1   0    0    1   
$EndComp
$Comp
L TLP2631-RESCUE-Power_converter_V4 OPT1
U 1 1 572226DE
P 5100 4800
F 0 "OPT1" H 5550 5700 60  0000 C CNN
F 1 "optocoupler" H 5500 4450 60  0000 C CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_LongPads" H 5100 5000 60  0001 C CNN
F 3 "" H 5100 5000 60  0000 C CNN
F 4 "Value" H 5100 4800 60  0000 C CNN "Bibliothèque/Application support/kicad/library/SHIELD_ARDUINO_empreintes"
F 5 "Value" H 5100 4800 60  0001 C CNN "Bibliothèque/Application support/kicad/library/Shield_Arduino_3D"
F 6 "Value" H 5100 4800 60  0001 C CNN "Fieldname"
	1    5100 4800
	1    0    0    -1  
$EndComp
$Comp
L R RO1
U 1 1 59367A12
P 3675 4050
F 0 "RO1" V 3775 4050 50  0000 C CNN
F 1 "220" V 3675 4050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 3605 4050 50  0001 C CNN
F 3 "" H 3675 4050 50  0000 C CNN
	1    3675 4050
	0    1    1    0   
$EndComp
$Comp
L R RO4
U 1 1 59367B80
P 4150 4850
F 0 "RO4" V 4250 4850 50  0000 C CNN
F 1 "220" V 4150 4850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4080 4850 50  0001 C CNN
F 3 "" H 4150 4850 50  0000 C CNN
	1    4150 4850
	0    1    1    0   
$EndComp
$Comp
L R R0H1
U 1 1 59368910
P 6200 3850
F 0 "R0H1" V 6300 3850 50  0000 C CNN
F 1 "2.2k" V 6200 3850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 6130 3850 50  0001 C CNN
F 3 "" H 6200 3850 50  0000 C CNN
	1    6200 3850
	1    0    0    -1  
$EndComp
$Comp
L R R0H2
U 1 1 59368965
P 6550 3850
F 0 "R0H2" V 6650 3850 50  0000 C CNN
F 1 "2.2k" V 6550 3850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 6480 3850 50  0001 C CNN
F 3 "" H 6550 3850 50  0000 C CNN
	1    6550 3850
	1    0    0    -1  
$EndComp
$Comp
L C CD2
U 1 1 5936923B
P 8200 4900
F 0 "CD2" H 8225 5000 50  0000 L CNN
F 1 "100n" H 8225 4800 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 8238 4750 50  0001 C CNN
F 3 "" H 8200 4900 50  0000 C CNN
	1    8200 4900
	1    0    0    -1  
$EndComp
$Comp
L C CD1
U 1 1 5936952A
P 7550 4650
F 0 "CD1" H 7575 4750 50  0000 L CNN
F 1 "100n" H 7250 4600 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 7588 4500 50  0001 C CNN
F 3 "" H 7550 4650 50  0000 C CNN
	1    7550 4650
	1    0    0    -1  
$EndComp
$Comp
L C CD3
U 1 1 59369B13
P 10200 4100
F 0 "CD3" H 10225 4200 50  0000 L CNN
F 1 "500n" H 10225 4000 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.0mm_P5.00mm" H 10238 3950 50  0001 C CNN
F 3 "" H 10200 4100 50  0000 C CNN
	1    10200 4100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 TH4
U 1 1 599F39EC
P 6700 1750
F 0 "TH4" H 6700 1850 50  0000 C CNN
F 1 "CONN_01X01" V 6800 1750 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6700 1750 50  0001 C CNN
F 3 "" H 6700 1750 50  0000 C CNN
	1    6700 1750
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 TH5
U 1 1 599F3A3B
P 6700 2000
F 0 "TH5" H 6700 2100 50  0000 C CNN
F 1 "CONN_01X01" V 6800 2000 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 6700 2000 50  0001 C CNN
F 3 "" H 6700 2000 50  0000 C CNN
	1    6700 2000
	-1   0    0    1   
$EndComp
Text Label 7200 1750 0    60   ~ 0
Hin
Text Label 7200 2000 0    60   ~ 0
Lin
$Comp
L R RO2
U 1 1 59367B05
P 4150 4350
F 0 "RO2" V 4250 4350 50  0000 C CNN
F 1 "220" V 4150 4350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 4080 4350 50  0001 C CNN
F 3 "" H 4150 4350 50  0000 C CNN
	1    4150 4350
	0    1    1    0   
$EndComp
$Comp
L R RO3
U 1 1 59367A7F
P 3675 4550
F 0 "RO3" V 3775 4550 50  0000 C CNN
F 1 "220" V 3675 4550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" H 3605 4550 50  0001 C CNN
F 3 "" H 3675 4550 50  0000 C CNN
	1    3675 4550
	0    1    1    0   
$EndComp
Text Notes 1625 3775 0    60   ~ 0
if PWMH or PWML= 5V peak, R0x = 220, if = 3.3V, R0x =120
Text Label 2550 1550 0    60   ~ 0
GNDPwR
Text Label 6050 5350 0    60   ~ 0
GNDPwR
Text Label 6950 4550 0    60   ~ 0
GNDPwR
Text Label 7050 5900 0    60   ~ 0
GNDPwR
Wire Notes Line
	700  1750 3550 1750
Wire Notes Line
	3550 1750 3550 650 
Wire Notes Line
	8100 2200 10950 2200
Wire Notes Line
	8100 1800 8100 700 
Wire Wire Line
	3825 4050 4450 4050
Wire Wire Line
	4300 4850 4450 4850
Wire Wire Line
	2425 4050 3525 4050
Wire Wire Line
	6050 4350 6050 5350
Wire Wire Line
	6200 3600 6200 3700
Wire Wire Line
	6550 3600 6550 3700
Connection ~ 6200 3600
Connection ~ 6200 4150
Wire Wire Line
	6725 4300 8550 4300
Wire Wire Line
	6950 4450 8550 4450
Wire Wire Line
	6950 4450 6950 4550
Wire Wire Line
	7050 4750 7050 5900
Wire Wire Line
	7050 4750 8550 4750
Wire Wire Line
	8550 4900 7200 4900
Wire Wire Line
	7200 4900 7200 5550
Wire Wire Line
	7200 5550 10500 5550
Wire Wire Line
	7550 5050 8550 5050
Wire Wire Line
	7750 3750 8900 3750
Wire Wire Line
	7750 3750 7750 5050
Connection ~ 7750 5050
Wire Wire Line
	9800 4300 9650 4300
Wire Wire Line
	9800 3750 9800 4300
Wire Wire Line
	9200 3750 9800 3750
Wire Wire Line
	10200 3800 10200 3950
Wire Wire Line
	10200 3800 9800 3800
Connection ~ 9800 3800
Wire Wire Line
	10200 4250 10200 4600
Wire Wire Line
	9650 4450 10450 4450
Connection ~ 7550 4450
Wire Wire Line
	7550 4800 7550 5050
Connection ~ 8200 4750
Connection ~ 8200 5050
Wire Wire Line
	9650 4600 10450 4600
Connection ~ 10200 4600
Wire Wire Line
	7050 5750 10500 5750
Connection ~ 7050 5750
Wire Wire Line
	1000 1050 1400 1050
Wire Wire Line
	1000 1300 1400 1300
Wire Wire Line
	1000 1550 1400 1550
Wire Wire Line
	8400 1000 9200 1000
Wire Wire Line
	8400 1200 9200 1200
Wire Wire Line
	8400 1400 9200 1400
Wire Wire Line
	8400 1600 9200 1600
Wire Wire Line
	5650 3600 6550 3600
Wire Wire Line
	2550 1400 2550 1550
Wire Wire Line
	2350 1400 2550 1400
Wire Wire Line
	6850 4150 6850 3350
Wire Wire Line
	6850 3350 7050 3350
Connection ~ 6850 4150
Wire Wire Line
	6375 4975 6800 4975
Wire Wire Line
	8400 1800 9200 1800
Wire Wire Line
	8400 2000 9200 2000
Wire Wire Line
	7650 5350 7850 5350
Wire Wire Line
	7850 5350 7850 5050
Connection ~ 7850 5050
Wire Wire Line
	2750 1000 2350 1000
Wire Wire Line
	2750 1200 2350 1200
Wire Wire Line
	3875 5550 1550 5550
Wire Wire Line
	6200 4000 6200 4150
Wire Wire Line
	7550 4500 7550 4450
Wire Wire Line
	6900 1750 7200 1750
Wire Wire Line
	6900 2000 7200 2000
Wire Wire Line
	2425 4550 3525 4550
Wire Wire Line
	4450 4350 4300 4350
Wire Wire Line
	3825 4550 4450 4550
Wire Wire Line
	6200 4150 8550 4150
Wire Wire Line
	4000 4350 3875 4350
Wire Wire Line
	3875 4350 3875 5550
Wire Wire Line
	3875 4850 4000 4850
Connection ~ 3875 4850
Wire Wire Line
	5750 4350 6050 4350
Wire Wire Line
	5750 4550 6725 4550
Wire Wire Line
	6550 4550 6550 4000
Wire Wire Line
	6375 4550 6375 4975
Connection ~ 6375 4550
Wire Wire Line
	6725 4550 6725 4300
Connection ~ 6550 4550
Wire Wire Line
	5750 4050 6200 4050
Connection ~ 6200 4050
Connection ~ 6050 4850
Wire Wire Line
	5750 4850 6050 4850
$EndSCHEMATC
