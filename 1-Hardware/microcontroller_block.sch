EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Power_converter_V4-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 7
Title ""
Date "20 apr 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1050 650  0    60   ~ 0
BLOCK INPUTS
Text Notes 800  750  0    60   ~ 0
----------------------------
Text HLabel 1104 1438 0    60   Input ~ 0
+5VLow
Text Label 1454 1438 2    60   ~ 0
+5VLow
Text Notes 8450 650  0    60   ~ 0
BLOCK OUTPUTS
Text Notes 8450 800  0    60   ~ 0
----------------------------
Text HLabel 908  1126 0    60   Input ~ 0
RO
Text HLabel 1000 950  0    60   Input ~ 0
miso
Text Label 1558 1126 2    60   ~ 0
RO
Text Label 1600 950  2    60   ~ 0
miso
Text Notes 3950 650  0    60   ~ 0
DETAILS OF THE MICROCONTROLLER BLOCK
Text Notes 3950 1400 0    60   ~ 0
This blocks connects the power converter with an \nArduino nano shield. The voltage and current \nmeasurements are inputs and the PWMs are the outputs.\n
Text Notes 3900 800  0    60   ~ 0
----------------------------
Text HLabel 1092 1646 0    60   Input ~ 0
GNDLow
Text Label 1492 1646 2    60   ~ 0
GNDLow
Text HLabel 9066 946  2    60   Output ~ 0
PWMH1
Text HLabel 9074 1132 2    60   Output ~ 0
PWML1
Text Label 8666 946  2    60   ~ 0
PWMH
Text Label 8674 1132 2    60   ~ 0
PWML
Text HLabel 10554 1334 2    60   Output ~ 0
GNDPWM
Text Label 10154 1334 2    60   ~ 0
GNDPWM
$Comp
L ARDUINO_NANO-RESCUE-Power_converter_V4 SH1
U 1 1 59529230
P 6000 4250
F 0 "SH1" H 5975 3175 60  0000 C CNN
F 1 "ARDUINO_NANO" H 5975 5300 60  0000 C CNN
F 2 "shield_arduino_footprint:ARDUINO_NANO_2_CONTOUR_trous_1MM!" H 5075 3650 60  0001 C CNN
F 3 "" H 5075 3650 60  0000 C CNN
	1    6000 4250
	1    0    0    -1  
$EndComp
Text Label 4600 3775 0    48   ~ 0
GNDPWM
Text Label 4800 4025 0    52   ~ 0
PWMH
Text Label 4800 4275 0    52   ~ 0
PWML
NoConn ~ 7200 3650
NoConn ~ 4800 4650
NoConn ~ 4800 4400
NoConn ~ 4800 4150
$Comp
L GND #PWR05
U 1 1 5973BF06
P 9050 4400
F 0 "#PWR05" H 9050 4150 50  0001 C CNN
F 1 "GND" H 9050 4250 50  0000 C CNN
F 2 "" H 9050 4400 50  0000 C CNN
F 3 "" H 9050 4400 50  0000 C CNN
	1    9050 4400
	1    0    0    -1  
$EndComp
Text Label 9800 4250 2    60   ~ 0
GNDPWM
Text Label 4800 4900 0    52   ~ 0
ss
Text Label 4800 5150 0    52   ~ 0
miso
Text Label 4800 5025 0    52   ~ 0
mosi
Text Label 7200 5150 2    52   ~ 0
clk
Text Label 4800 3900 0    52   ~ 0
DE/RE
Text Label 4600 3525 0    52   ~ 0
RO
Text Label 4800 3400 0    52   ~ 0
DI
Text HLabel 9058 1300 2    60   Output ~ 0
ss
Text Label 8658 1300 2    60   ~ 0
ss
Text HLabel 9050 1452 2    60   Output ~ 0
mosi
Text Label 8650 1452 2    60   ~ 0
mosi
Text HLabel 9074 1620 2    60   Output ~ 0
clk
Text Label 8674 1620 2    60   ~ 0
clk
Text HLabel 10242 932  2    60   Output ~ 0
DI
Text Label 9842 932  2    60   ~ 0
DI
Text HLabel 10258 1124 2    60   Output ~ 0
DE/RE
Text Label 9858 1124 2    60   ~ 0
DE/RE
NoConn ~ 4800 3650
NoConn ~ 7200 4150
NoConn ~ 7200 4275
NoConn ~ 7200 4400
NoConn ~ 7200 4525
NoConn ~ 7200 4650
NoConn ~ 7200 4775
NoConn ~ 7200 4900
NoConn ~ 7200 5025
NoConn ~ 7200 3400
NoConn ~ 4800 4525
NoConn ~ 4800 4775
NoConn ~ 7700 4025
NoConn ~ 7700 3900
Text Label 7700 3775 2    60   ~ 0
+5VLow
Text Label 7200 3525 2    60   ~ 0
GNDLow
Wire Wire Line
	10554 1334 10154 1334
Wire Wire Line
	9074 1132 8674 1132
Wire Wire Line
	9066 946  8666 946 
Wire Notes Line
	8200 1750 8200 1700
Wire Notes Line
	3900 1750 8200 1750
Wire Wire Line
	1092 1646 1492 1646
Wire Wire Line
	1000 950  1600 950 
Wire Wire Line
	908  1126 1558 1126
Wire Notes Line
	8250 1750 8250 650 
Wire Notes Line
	8250 1750 11100 1750
Wire Notes Line
	3800 1750 3800 500 
Wire Notes Line
	700  1750 3800 1750
Wire Wire Line
	1104 1438 1454 1438
Wire Wire Line
	7200 4775 6650 4775
Wire Wire Line
	7200 4525 6650 4525
Wire Wire Line
	7200 4275 6650 4275
Wire Wire Line
	6650 3650 7200 3650
Wire Wire Line
	7200 3525 6650 3525
Wire Wire Line
	6650 3775 7700 3775
Wire Wire Line
	7700 4025 6650 4025
Wire Wire Line
	4800 5025 5350 5025
Wire Wire Line
	4800 4775 5350 4775
Wire Wire Line
	4600 3775 5350 3775
Wire Wire Line
	5350 3525 4600 3525
Wire Wire Line
	5350 4525 4800 4525
Wire Wire Line
	5350 4275 4800 4275
Wire Wire Line
	5350 4025 4800 4025
Wire Wire Line
	4800 3900 5350 3900
Wire Wire Line
	6650 3400 7200 3400
Wire Wire Line
	7200 5025 6650 5025
Wire Wire Line
	4800 4400 5350 4400
Wire Wire Line
	6650 4150 7200 4150
Wire Wire Line
	6650 3900 7700 3900
Wire Wire Line
	6650 4650 7200 4650
Wire Wire Line
	6650 5150 7200 5150
Wire Wire Line
	6650 4900 7200 4900
Wire Wire Line
	6650 4400 7200 4400
Wire Wire Line
	5350 4150 4800 4150
Wire Wire Line
	5350 3650 4800 3650
Wire Wire Line
	5350 3400 4800 3400
Wire Wire Line
	5350 4650 4800 4650
Wire Wire Line
	5350 4900 4800 4900
Wire Wire Line
	5350 5150 4800 5150
Wire Wire Line
	9058 1300 8658 1300
Wire Wire Line
	9050 1452 8650 1452
Wire Wire Line
	9074 1620 8674 1620
Wire Wire Line
	10242 932  9842 932 
Wire Wire Line
	10258 1124 9858 1124
Wire Wire Line
	9050 4400 9050 3910
Wire Wire Line
	9050 3910 9780 3910
Wire Wire Line
	9800 4250 9050 4250
Connection ~ 9050 4250
Text Label 9780 3910 2    60   ~ 0
GNDLow
$EndSCHEMATC
